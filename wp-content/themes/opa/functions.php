<?php

Function qode_child_theme_enqueue_scripts() {
	wp_register_style( 'childstyle', get_stylesheet_directory_uri() . '/style.css'  );
	wp_enqueue_style( 'childstyle' );
}
add_action( 'wp_enqueue_scripts', 'qode_child_theme_enqueue_scripts', 11);

function qode_child_theme_admin_enqueue_scripts() {
	wp_register_style( 'childstyleAdmin', get_stylesheet_directory_uri() . '/admin_style.css'  );
	wp_enqueue_style( 'childstyleAdmin' );
	wp_register_script('childJSAdmin', get_stylesheet_directory_uri() . '/admin_app.js', array('jquery'), true, false );
	wp_enqueue_script( 'childJSAdmin');
}
add_action( 'admin_enqueue_scripts', 'qode_child_theme_admin_enqueue_scripts');

add_action('wp_logout','auto_redirect_after_logout');
function auto_redirect_after_logout(){
  wp_redirect( home_url() );
  exit();
}

// show admin bar only for admins and editors
if (!current_user_can('edit_posts')) {
	add_filter('show_admin_bar', '__return_false');
}

/*add_filter( 'portfolio_page_post_type_args', '_my_rewrite_slug' );
  function _my_rewrite_slug( $args ) {
  $args['rewrite']['slug'] = 'portfolios';
  return $args;
}*/

function opa_show_filters($filter = '') {
	$params = array(
		'taxonomy' => 'wpdmcategory',
    'orderby' => 'name',
    'order'   => 'ASC'
	);
	$categories = get_categories( $params );
	if ($filter=='' || $filter=='all') {
		$current = " current";
		$url = "#";
	} else {
		$current = "";
		$url = get_page_link();
	}
	echo "<li class='filter$current' data-filter='.portfolio_category_all'><span><a href='$url'>Todos</a></span></li>";
	foreach ($categories as $cat) {
		if ($cat->slug==$filter) {
			$current = " current";
			$url = "#";
		} else {
			$current = "";
			$url = get_page_link()."?filter=".$cat->slug;
		}
		echo "<li class='filter$current' data-filter='.portfolio_category_".$cat->term_id."'><span><a href='$url'>".$cat->name."</a></span></li>";
	}
}

if (!function_exists('pR')) {
	function pR($t) {
		echo "<pre>";
		print_r($t);
		echo "</pre>";
	}
}

// Shortcode da opa para filtros da biblioteca
function opa_show_download_list_filters() {
	ob_start();
	?>
	<div class="filter_outer">
		<div class="filter_holder">
			<ul>
				<?php opa_show_filters($_GET['filter']); ?>
			</ul>
		</div>
	</div>
	<?php
	$data = ob_get_clean();

	return $data;
}
add_shortcode('opa_show_download_list_filters', 'opa_show_download_list_filters');


add_action( 'wpas_open_ticket_after', 'wpas_notify_support_managers', 11, 2 );
function wpas_notify_support_managers( $ticket_id, $data ) {
	opa_email_notify( $ticket_id, 'submission_confirmation_to_support_managers' );
}

/**
 * OPA Wrapper function to trigger an e-mail notification.
 *
 * @since  3.0.2
 * @param  integer         $post_id ID of the post to notify about
 * @param  string|array    $cases   The case(s) to notify for
 * @return boolean|object           True if the notification was sent, WP_Error or false otherwise
 */
function opa_email_notify( $post_id, $cases ) {

	if ( is_array( $cases ) ) {

		foreach ( $cases as $case ) {
			if ( !opa_notify( $post_id, $case ) ) {
				$error = true;
			}
		}

		return true === $error ? false : true;

	} else {
		return opa_notify( $post_id, $cases );
	}

}


/**
 * Send out the e-mail notification.
 *
 * @since  3.0.2
 * @param  string         $case The notification case
 * @return boolean|object       True if the notification was sent, WP_Error otherwise
 */
function opa_notify( $post_id, $case ) {
	$emails = new WPAS_Email_Notification( $post_id );
	// Define the $user var to avoid undefined var notices when using a custom $case
	$user = null;

	/**
	 * Find out who's the user to notify
	 */


	switch ( $case ) {
		case 'submission_confirmation_to_support_managers':
			//$user = get_users( array( 'role' => 'wpas_support_manager', 'fields' => 'all' ) );
			$user = get_users( array( 'role' => 'wpas_support_manager', 'fields' => 'all' ) );
			break;
	}

	$recipients = $recipient_emails = array();
	if (is_array($user)) {
		$recipients = array_merge($recipients, $user);
	} else {
		$recipients[] = $user;
	}

	foreach( $recipients as $recipient ) {
		if( $recipient instanceof WP_User ) {
			$recipient_emails[] = array( 'user_id' => $recipient->ID, 'email' => $recipient->user_email );
		}

	}

	/**
	 * Get the sender information
	 */
	 $sender = array(
	 	'from_name'   => stripslashes( wpas_get_option( 'sender_name', get_bloginfo( 'name' ) ) ),
	 	'from_email'  => wpas_get_option( 'sender_email', get_bloginfo( 'admin_email' ) ),
	 	'reply_email' => wpas_get_option( 'reply_email', get_bloginfo( 'admin_email' ) ),
	 );

	 $sender['reply_name']  = $sender['from_name'];

	$from_name   = $sender['from_name'];
	$from_email  = $sender['from_email'];
	$reply_name  = $sender['reply_name'];
	$reply_email = $sender['reply_email'];

	/**
	 * Get e-mail subject
	 *
	 * @var  string
	 */
	$pre_fetch = apply_filters( 'wpas_email_notifications_pre_fetch_' . 'subject', wpas_get_option( "subject_assignment", "" ), $post_id, 'new_ticket_assigned' );
	$subject = stripslashes( $emails->fetch($pre_fetch) );

	/**
	 * Get the e-mail body and filter it before the template is being applied
	 *
	 * @var  string
	 */
	$pre_fetch = apply_filters( 'wpas_email_notifications_pre_fetch_' . 'content', wpas_get_option( "content_assignment", "" ), $post_id, 'new_ticket_assigned' );
	$body = stripslashes( $emails->fetch($pre_fetch) );

	/**
	 * Filter the e-mail body after the template has been applied
	 *
	 * @since 3.3.3
	 * @var string
	 */
	$body = apply_filters( 'wpas_email_notification_body_after_template', $emails->get_formatted_email( $body ), 'new_ticket_assigned', $post_id );

	/**
	 * Prepare e-mail headers
	 *
	 * @var array
	 */
	$headers = array(
		"MIME-Version: 1.0",
		"Content-type: text/html; charset=utf-8",
		"From: $from_name <$from_email>",
		"Reply-To: $reply_name <$reply_email>",
		// "Subject: $subject",
		"X-Mailer: Awesome Support/" . WPAS_VERSION,
	);

	/**
	 * Merge all the e-mail variables and apply the wpas_email_notifications_email filter.
	 */
	$email = apply_filters( 'wpas_email_notifications_email', array(
		'recipient_email' => $recipient_emails,
		'subject'         => $subject,
		'body'            => $body,
		'headers'         => $headers,
		'attachments'     => ''
		),
		$case,
		$post_id
	);

	$attachments = array();

	if( !is_array( $email['recipient_email'] ) ) {
		$email['recipient_email'] = array( $email['recipient_email'] );
	}


	// We need to send notifications separately per recipient.
	$mail = false;

	$email_sent_recipients = array();

	foreach( $email['recipient_email'] as $r_email ) {

		$email_headers = $email['headers'];

		$to_email = $r_email;

		if( is_array( $r_email ) &&  isset( $r_email['email'] ) && $r_email['email'] ) {
			$to_email = $r_email['email'];
		}

		/* Make sure that the email is not already in the array - don't want dupes! */
		if( in_array( $to_email, $email_sent_recipients ) ) {
			continue;
		}

		$email_sent_recipients[] = $to_email;

		if( is_array( $r_email ) && isset( $r_email['cc_addresses'] ) && !empty( $r_email['cc_addresses'] ) ) {
			$email_headers[] = 'Cc: ' . implode( ',', $r_email['cc_addresses'] );
		}

		if( wp_mail( $to_email, $email['subject'], $email['body'], $email_headers, $attachments ) ) {
			$mail = true;
		}
	}

	return $mail;

}

function opa_get_trimmed_title($title, $limit=0) {
	if ($limit>0) {
		$ss = substr($title, 0, $limit);
		$tmp = explode(" ", substr($title, $limit));
		$bw = array_shift($tmp);
		$ss .= $bw;
		$title_final = ($ss != $title)
			? $ss . '...'
			: $ss;
	} else {
		$title_final = $title;
	}
	return $title_final;
}
