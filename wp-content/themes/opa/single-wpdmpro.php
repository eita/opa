<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php
		while ( have_posts() ) : the_post();

      ?>
      <div class="title_outer title_without_animation">
		      <div class="title title_size_small  position_left ">
			         <div class="image not_responsive"></div>
							 <div class="title_holder" style="height:100px;">
		                <div class="container">
						                <div class="container_inner clearfix">
								                      <div class="title_subtitle_holder">
                                        <h1>
                                          <span><?php the_title(); ?></span>
                                        </h1>
																			  <span class="separator small left"></span>
                                      </div>
                            </div>
                    </div>
	             </div>
					</div>
			</div>

      <div class="container">
          <div class="container_inner clearfix">
          	<?php the_content(); ?>
					</div>
      </div>
    <?php
		endwhile;
		?>

		</main>
	</div>

<?php get_footer(); ?>
