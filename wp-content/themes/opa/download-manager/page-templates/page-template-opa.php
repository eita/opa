<!-- WPDM Template: OPA -->

<div class="row">
    <div class="col-md-4 col-lg-3 col-sm-12">
        [thumb_800x600]
    </div>

    <div class="col-md-8 col-lg-9 col-sm-12">
        <div class="">
          [download_link_extended]
        </div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#wpdmpp-product-desc" aria-controls="wpdmpp-product-desc" role="tab" data-toggle="tab">[txt=Descrição]</a></li>
            <li role="presentation"><a href="#wpdmpp-product-info" aria-controls="wpdmpp-product-info" role="tab" data-toggle="tab">[txt=Sobre o arquivo]</a></li>


        </ul>

        <!-- Tab panes -->
        <div class="tab-content" style="padding: 15px 0">
            <div role="tabpanel" class="tab-pane active" id="wpdmpp-product-desc">[description]</div>
            <div role="tabpanel" class="tab-pane" id="wpdmpp-product-info">
                <ul class="list-group">
                    <li class="list-group-item [hide_empty:icon]">
                      <span>[icon]</span>
                      [txt=Tipo de arquivo]
                    </li>
                    <li class="list-group-item [hide_empty:download_count]">
                        <span class="badge">[download_count]</span>
                        [txt=Downloads]
                    </li>
                    <li class="list-group-item [hide_empty:file_size]">
                        <span class="badge">[file_size]</span>
                        [txt=Tamanho]
                    </li>
                    <li class="list-group-item [hide_empty:create_date]">
                        <span class="badge">[create_date]</span>
                        [txt=Data de criação]
                    </li>
                    <li class="list-group-item [hide_empty:update_date]">
                        <span class="badge">[update_date]</span>
                        [txt=Última atualização]
                    </li>

                </ul>
            </div>

        </div>


    </div>


</div>
<script>
    jQuery(function ($) {
        $('.nav-tabs').tabs();
    });
</script>
