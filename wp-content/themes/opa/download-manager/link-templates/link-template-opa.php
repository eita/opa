<!-- WPDM Link Template: OPA -->

<div class="well c2a4">
    <div class="media text-center">
        <div class="wicon-wrapper">
          <div class="text-center wicon" style="width: 100%;margin: 5px 0">
            <a href="[page_url]" title="[title]">
            [thumb_700x467]
            </a>
          </div>
          <div class="file_type-icon">
            [icon]
          </div>
        </div>
        <div class="media-body">
            <h3 class="media-heading" style="padding: 0;margin: 10px 0;"><a style="font-weight: 700;font-size:14pt;" href="[page_url]" title="[title]">[title_50]</a></h3>
            <p class="text-left opa_wpdm_excerpt">[excerpt_120]</p>
            <div class="text-info" style="font-weight:300;font-size: 10pt;margin-bottom: 15px"><i class="fa fa-database color-green"></i> [file_size] <i class="fa fa-download color-purple"></i> [download_count] downloads</div>
            <div class="text-info" style="font-weight:300;font-size: 10pt;margin-bottom: 15px">
              [categories]
            </div>
        </div>
        <div class="text-center">
            [download_link]
        </div>

    </div>

</div>
