��    (      \  5   �      p     q     �     �     �     �     �     �     �          *     =     Y     l     �     �     �     �  	   �     �               %     3  	   B  	   L  '   V     ~  '   �  ,   �     �     �          #     9     @     Z     o     t     w  z  z  &   �  %         B  !   c     �     �     �     �     �     �  $   	     4	     P	     o	     �	     �	     �	  
   �	     �	     �	     
     
     -
     @
  
   N
  /   Y
  0   �
  (   �
  -   �
          '     .     N     l  !   s     �     �     �     �     "                (   '                  &             %                                    
      !       $                                 	                             #          Add New Portfolio Category Add New Portfolio Item Add New Portfolio Tag All Portfolio Categories All Portfolio Tags Articles posted by  Edit Edit Portfolio Category Edit Portfolio Item Edit Portfolio Tag New Portfolio Category Name New Portfolio Item New Portfolio Tag Name Parent Portfolio Category Parent Portfolio Category: Parent Portfolio Tag Parent Portfolio Tags: Portfolio Portfolio Categories Portfolio Category Portfolio Item Portfolio Tag Portfolio Tags Posted at Posted in Qode Additional Portfolio Sidebar Items Qode Portfolio General Qode Portfolio Images (multiple upload) Qode Portfolio Images/Videos (single upload) Related Projects Search Here Search Portfolio Categories Search Portfolio Tags Submit Update Portfolio Category Update Portfolio Tag View by in Project-Id-Version: Bridge 14.5
Report-Msgid-Bugs-To: https://wordpress.org/support/theme/bridge
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-03-29 11:58-0300
X-Generator: Poedit 2.0.6
Plural-Forms: nplurals=2; plural=(n > 1);
Language-Team: 
Last-Translator: daniel tygel <dtygel@eita.org.br>
Language: pt_BR
 Adicionar nova categoria da biblioteca Adicionar novo item para a biblioteca Adicionar novo tag da biblioteca Todas as categorias da biblioteca Todas as tags da biblioteca Artigos postados por Editar Editar categoria da biblioteca Editar item da biblioteca Editar tag da biblioteca Novo nome de categoria da biblioteca Novo item para a biblioteca Novo nome de tag da biblioteca Categoria-mãe da biblioteca Categoria-mãe: Tag-mãe da biblioteca Tags-mães da biblioteca Biblioteca Categorias da biblioteca Categoria da biblioteca Item da biblioteca Tag da biblioteca Tags da biblioteca Publicado às Postado em Itens adicionais da barra lateral de biblioteca Imagens/Vídeos da biblioteca (upload múltiplo) Imagens da biblioteca (upload múltiplo) Imagens/Vídeos da biblioteca (upload único) Projetos relacionados Buscar Buscar categorias da biblioteca Buscar por tags da biblioteca Enviar Atualizar categoria da biblioteca Atualizar tag da biblioteca ver por em 