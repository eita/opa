<?php
/**
 * @package   Awesome Support Notifications
 * @author    Awesome Support <contact@getawesomesupport.com>
 * @license   GPL-2.0+
 * @link      https://getawesomesupport.com
 * @copyright 2016 - 2017 Awesome Support
 *
 * @wordpress-plugin
 * Plugin Name:       Awesome Support: Notifications
 * Plugin URI:        http://getawesomesupport.com/addons/notifications/?utm_source=internal&utm_medium=plugin_meta&utm_campaign=Addons_Notifications
 * Description:       Get notified of new tickets and replies through your favorite communication channels.
 * Version:           2.1.0
 * Author:            The Awesome Support Team
 * Author URI:        https://getawesomesupport.com?utm_source=internal&utm_medium=plugin_meta&utm_campaign=Addons_Notifications
 * Text Domain:       as-instant-notifications
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/* 
 * Include extension base class and make sure that the Awesome Support plugin is enabled.
 *
 * To do this, we perform checks in the following order:
 *  1. Check if the WPAS_Extension_Base class is available.  
 *  2. If not, check if the variable WPAS_ROOT is defined and the file is available in that path.
 *  3. If not, check if the variable WPAS_AS_FOLDER is defined and the file is available in that path.
 * 
 * If the file does not exist, we throw up an error message.
 */
if ( !class_exists( 'WPAS_Extension_Base' ) ) {
	
	$wpas_dir = defined( 'WPAS_ROOT' )  ? WPAS_ROOT : ( defined( 'WPAS_AS_FOLDER' ) ? WPAS_AS_FOLDER : 'awesome-support' );
	$wpas_eb_file = trailingslashit( WP_PLUGIN_DIR . '/' . $wpas_dir ) . 'includes/class-extension-base.php';
	
	if( file_exists( $wpas_eb_file ) ) {
		require_once ( $wpas_eb_file );
	} else {
		add_action( 'admin_notices', function() {
		?>	
		
		<div class="error">
			<p>
				<?php printf( __( 'The LATEST version of Awesome Support needs to be installed in order to activate the Notifications add-on for Awesome Support. Please <a href="%s" target="_blank">install Awesome Support</a> before continuing.', 'as-instant-notifications' ), esc_url( 'https://getawesomesupport.com' ) ); ?>
			</p>
		</div>
			
		<?php	
			
		});
		
		return;
	}
}

/*----------------------------------------------------------------------------*
 * Instantiate the plugin
 *----------------------------------------------------------------------------*/

/**
 * Register the activation hook
 */
register_activation_hook(   __FILE__, array( 'AS_Notification_Loader', 'activate'   ) );

add_action( 'plugins_loaded', array( 'AS_Notification_Loader', 'get_instance' ), 9 );


/**
 * Instantiate the addon.
 *
 * This method runs a few checks to make sure that the addon
 * can indeed be used in the current context, after what it
 * registers the addon to the core plugin for it to be loaded
 * when the entire core is ready.
 *
 * @since  0.1.0
 * @return void
 */
class AS_Notification_Loader extends WPAS_Extension_Base {

	/**
	 * Instance of this loader class.
	 *
	 * @since    0.1.0
	 * @var      object
	 */
	protected static $instance = null;
	
	/**
	 * addon construct method
	 */
	public function __construct() {
		
		$this->setVersionRequired( '5.5.2' );				// Set required version of core
		$this->setPhpVersionRequired( '5.6' );				// Set required version of php
		$this->setSlug( 'notifications' );					// Set addon slug
		$this->setUid( 'NOT' );								// Set short unique id
		$this->setTextDomain( 'as-instant-notifications' ); // Set text domain for translation
		$this->setVersion( '2.1.0' );						// Set addon version
		$this->setItemId( 318 );							// Set addon item id
		
		parent::__construct();
	}
	
	/**
	 * Load required files
	 */
	public function load() {

		require_once( WPAS_NOT_PATH . 'inc/functions.php' );
		
		require_once( WPAS_NOT_PATH . 'class-notifications.php' );

		/**
		 * Load all the integrations.
		 */
		require_once( WPAS_NOT_PATH . 'inc/class-slack.php' );
		require_once( WPAS_NOT_PATH . 'inc/class-pushover.php' );
		require_once( WPAS_NOT_PATH . 'inc/class-pushbullet.php' );
		require_once( WPAS_NOT_PATH . 'inc/class-email.php' );

		/**
		 * Load the settings
		 */
		require_once( WPAS_NOT_PATH . 'settings.php' );

		/**
		 * Load main classes
		 */
		require_once( WPAS_NOT_PATH . 'class-notify.php' );

		/**
		 * Load vendor dependencies
		 */
		if ( $this->dependencies_available() ) {
			require_once( WPAS_NOT_PATH . 'vendor/autoload.php' );
		}
		
		WPAS_Notifications::get_instance();

	}
	
	/**
	 * Run right after addon is registered
	 */
	protected function after_init() {
		
		if ( is_admin() ) {
			add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( 'WPAS_Notifications', 'settings_page_link' ) );
		}
	}

	/**
	 * Check if the vendor dependencies are available
	 *
	 * @since 0.1.3
	 * @return bool
	 */
	protected function dependencies_available() {
		return file_exists( WPAS_NOT_PATH . 'vendor/autoload.php' ) ? true : false;
	}

}