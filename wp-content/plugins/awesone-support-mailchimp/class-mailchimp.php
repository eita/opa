<?php
/**
 * MailChimp
 *
 * @package   Awesome Support MailChimp
 * @author    Awesome Support <contact@getawesomesupport.com>
 * @license   GPL-2.0+
 * @link      https://getawesomesupport.com
 * @copyright 2016 Awesome Support
 */

class WPAS_MailChimp {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Holds the instance of the MailChimp API wrapper
	 *
	 * @since 0.2.0
	 * @var null|DrewM\MailChimp\MailChimp
	 */
	protected $mailchimp;

	public function __construct() {

		$this->api_key       = wpas_get_option( 'mailchimp_api_key', '' );
		$this->list_id       = wpas_get_option( 'mailchimp_list_id', '' );
		$this->double_optin  = wpas_get_option( 'mailchimp_double_optin', true );
		$this->update        = wpas_get_option( 'mailchimp_update_existing', true );
		$this->welcome       = wpas_get_option( 'mailchimp_welcome', true );

		// Instantiate the MailChimp API wrapper.
		if ( false !== $this->api_key && '' !== $this->api_key ) {

			// Load the MailChimp API wrapper.
			if ( ! class_exists( '\DrewM\MailChimp\MailChimp' ) ) {
				require( 'vendor/drewm/mailchimp-api/src/MailChimp.php' );
			}

			$this->mailchimp = new \DrewM\MailChimp\MailChimp( $this->api_key );

		}

		// Load the plugin translation.
		add_action( 'plugins_loaded', array( $this, 'load_plugin_textdomain' ), 15 );

		if ( !is_admin() ) {
			add_action( 'wpas_after_registration_fields', array( $this, 'subscribe_checkbox' ), 10, 0 );
			add_action( 'wpas_register_account_after',    array( $this, 'subscribe' ), 10, 2 );
		}

	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     3.0.0
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Activate the plugin.
	 *
	 * The activation method just checks if the main plugin
	 * Awesome Support is installed (active or inactive) on the site.
	 * If not, the addon installation is aborted and an error message is displayed.
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public static function activate() {

		if ( !class_exists( 'Awesome_Support' ) ) {
			deactivate_plugins( basename( __FILE__ ) );
			wp_die(
				sprintf( __( 'You need Awesome Support to activate this addon. Please <a href="%s" target="_blank">install Awesome Support</a> before continuing.', 'as-mailchimp' ), esc_url( 'https://getawesomesupport.com' ) )
			);
		}

	}

	/**
	 * Get user lists.
	 *
	 * @since  1.0.0
	 * @return array Array of available lists for this account
	 */
	public function get_lists() {

		if ( null === $this->mailchimp ) {
			return array();
		}

		$lists = $this->mailchimp->get( 'lists' );

		return $lists;

	}

	/**
	 * Display the subscription checkbox.
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function subscribe_checkbox() {

		if( empty( $this->api_key ) || empty( $this->list_id ) ) {
			return;
		}

		$label   = apply_filters( 'wpasmc_subscribe_label', __( 'Subscribe to the newsletter', 'as-mailchimp' ) );
		$checked = boolval( wpas_get_option( 'mailchimp_checked', false ) ); ?>
		<div class="wpas-checkbox">
			<label><input type="checkbox" name="mailchimp_subscribe" value="1" <?php if ( $checked ): ?>checked="checked"<?php endif; ?>> <?php echo $label; ?></label>
		</div>
	<?php }

	/**
	 * Add the new user to the MailChimp list.
	 *
	 * @since  0.1.0
	 * @return mixed Subscription status
	 */
	public function subscribe( $user_id, $data ) {

		/* Abort if the user doesn't want to subscribe. */
		if ( !isset( $_POST['mailchimp_subscribe'] ) || '1' !== $_POST['mailchimp_subscribe'] ) {
			return false;
		}

		if ( null === $this->mailchimp ) {
			return false;
		}

		$list_id     = $this->list_id;
		$optin       = (bool) $this->double_optin;
		$merge_vars  = $this->merge_vars( $user_id, $data );
		$settings = array(
			'email_address' => sanitize_email( $data['email'] ),
			'status'        => true === $optin ? 'pending' : 'subscribed',
			'merge_fields'    => $merge_vars,
			'email_type'    => 'html',
		);

		// Call the API.
		return $this->mailchimp->post( "lists/$list_id/members", $settings );

	}

	public function merge_vars( $user_id, $data ) {

		$vars = array();

		if ( isset( $data['first_name'] ) ) {
			$vars['FNAME'] = $data['first_name'];
		}

		if ( isset( $data['first_name'] ) ) {
			$vars['LNAME'] = $data['last_name'];
		}
		
		return apply_filters( 'wpasmc_merge_vars', $vars, $user_id, $data );

	}

	/**
	 * User warning.
	 *
	 * Warn user if settings are not correct.
	 *
	 * @since  0.1.0
	 */
	public static function settings_warning() {

		$api_key = wpas_get_option( 'mailchimp_api_key', '' );
		$list_id = wpas_get_option( 'mailchimp_list_id', '' );

		if ( empty( $api_key ) || empty( $list_id ) ):

			$text = empty( $api_key ) ? _x( 'specify your API key', 'MailChimp setup incomplete', 'as-mailchimp' ) : _x( 'select a list', 'MailChimp setup incomplete', 'as-mailchimp' );
			$url = wpas_get_settings_page_url( 'mailchimp' ); ?>
	    
			<div class="error">
				<p><?php printf( __( 'MailChimp add-on for Awesome Support is not correctly setup. You didn&#39; %s. <a href="%s">Click here to see the settings</a>.', 'as-mailchimp' ), $text, $url ); ?></p>
			</div>

		<?php endif;
	    
	}

	/**
	 * Load the plugin text domain for translation.
	 *
	 * With the introduction of plugins language packs in WordPress loading the textdomain is slightly more complex.
	 *
	 * We now have 3 steps:
	 *
	 * 1. Check for the language pack in the WordPress core directory
	 * 2. Check for the translation file in the plugin's language directory
	 * 3. Fallback to loading the textdomain the classic way
	 *
	 * @since   0.1.2
	 * @return boolean True if the language file was loaded, false otherwise
	 */
	public function load_plugin_textdomain() {

		$lang_dir       = WPASMC_ROOT . 'languages/';
		$lang_path      = WPASMC_PATH . 'languages/';
		$locale         = apply_filters( 'plugin_locale', get_locale(), 'as-mailchimp' );
		$mofile         = "as-mailchimp-$locale.mo";
		$glotpress_file = WP_LANG_DIR . '/plugins/awesome-support-mailchimp/' . $mofile;

		// Look for the GlotPress language pack first of all
		if ( file_exists( $glotpress_file ) ) {
			$language = load_textdomain( 'as-mailchimp', $glotpress_file );
		} elseif ( file_exists( $lang_path . $mofile ) ) {
			$language = load_textdomain( 'as-mailchimp', $lang_path . $mofile );
		} else {
			$language = load_plugin_textdomain( 'as-mailchimp', false, $lang_dir );
		}

		return $language;

	}

}