<?php
add_filter( 'wpas_plugin_settings', 'wpas_settings_mailchimp', 10, 1 );
/**
 * Add settings for MailChimp addon.
 *
 * @param  array $def Array of existing settings
 *
 * @return array      Updated settings
 */
function wpas_settings_mailchimp( $def ) {

	$mc    = WPAS_MailChimp::get_instance();
	$lists = $mc->get_lists();

	if ( false === $lists ) {

		$list_id = array(
			'name'    => __( 'List ID', 'as-mailchimp' ),
			'id'      => 'mailchimp_list_id',
			'type'    => 'text',
			'default' => '',
			'desc'    => sprintf( __( 'Input your API key and save if you want to see a dropdown of all your lists. If you don\'t know how to get your API key please <a href="%s" target="_blank">read this documentation</a>.', 'as-mailchimp' ), esc_url( 'http://eepurl.com/im9k' ) ),
		);

	} else {

		if ( isset( $lists['lists'] ) && is_array( $lists['lists'] ) ) {

			$opts[''] = __( 'Please select...', 'as-mailchimp' );

			foreach ( $lists['lists'] as $key => $list ) {
				$opts[ $list['id'] ] = $list['name'];
			}

			$list_id = array(
				'name'    => __( 'List ID', 'as-mailchimp' ),
				'id'      => 'mailchimp_list_id',
				'type'    => 'select',
				'options' => $opts,
				'default' => '',
			);

		} else {

			$list_id = array(
				'name'    => __( 'List ID', 'as-mailchimp' ),
				'id'      => 'mailchimp_list_id',
				'type'    => 'text',
				'default' => '',
				'desc'    => __( 'Input your API key and save if you want to see a dropdown of all your lists.', 'as-mailchimp' ),
				'default' => '',
			);

		}

	}

	$settings = array(
		'mailchimp' => array(
			'name'    => __( 'MailChimp', 'as-mailchimp' ),
			'options' => array(
				array(
					'name'    => __( 'API Key', 'as-mailchimp' ),
					'id'      => 'mailchimp_api_key',
					'type'    => 'text',
					'default' => '',
					'desc'    => sprintf( __( 'If you don\'t know how to get your API key please <a href="%s" target="_blank">read this documentation</a>.', 'as-mailchimp' ), esc_url( 'http://eepurl.com/im9k' ) ),
				),
				$list_id,
				array(
					'name'    => __( 'Checked by Default', 'as-mailchimp' ),
					'id'      => 'mailchimp_checked',
					'type'    => 'checkbox',
					'default' => false,
					'desc'    => __( 'Check the checkbox by default', 'as-mailchimp' ),
				),
				array(
					'name'    => __( 'Double-Optin', 'as-mailchimp' ),
					'id'      => 'mailchimp_double_optin',
					'type'    => 'checkbox',
					'default' => true,
					'desc'    => __( 'MailChimp asks for a subscription confirmation. It is advised NOT to disable it.', 'as-mailchimp' ),
				),
			),
		),
	);

	return array_merge( $def, $settings );

}