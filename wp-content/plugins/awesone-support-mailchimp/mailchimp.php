<?php
/**
 * @package   Awesome Support MailChimp
 * @author    Awesome Support <contact@getawesomesupport.com>
 * @license   GPL-2.0+
 * @link      https://getawesomesupport.com
 * @copyright 2016 Awesome Support
 *
 * @wordpress-plugin
 * Plugin Name:       Awesome Support: MailChimp
 * Plugin URI:        http://getawesomesupport.com/addons/mailchimp/
 * Description:       Add MailChimp newsletter subscription checkbox on the registration form.
 * Version:           0.2.0
 * Author:            The Awesome Support Team
 * Author URI:        https://getawesomesupport.com
 * Text Domain:       as-mailchimp
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/*----------------------------------------------------------------------------*
 * Shortcuts
 *----------------------------------------------------------------------------*/

define( 'WPASMC_VERSION', '0.2.0' );
define( 'WPASMC_URL', trailingslashit( plugin_dir_url( __FILE__ ) ) );
define( 'WPASMC_PATH', trailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'WPASMC_ROOT', trailingslashit( dirname( plugin_basename( __FILE__ ) ) ) );

/*----------------------------------------------------------------------------*
 * Instantiate the plugin
 *----------------------------------------------------------------------------*/

require_once( 'class-mailchimp.php' );
register_activation_hook( __FILE__, array( 'WPAS_MailChimp', 'activate' ) );

/**
 * Check if Awesome Support is active
 **/
if ( in_array( 'awesome-support/awesome-support.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	add_action( 'plugins_loaded', array( 'WPAS_MailChimp', 'get_instance' ), 10, 0 );
	add_action( 'admin_notices',  array( 'WPAS_MailChimp', 'settings_warning' ), 10, 0 );

	// Load the addon settings.
	require_once( 'settings-mailchimp.php' );

}