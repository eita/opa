��          L      |       �      �      �   !   �   X   �      E  �  Z     ,     C  +   _  z   �  0                                            Add Note Add Note &amp; Transfer Add a private note to this ticket This note will only be seen by you and other agents. Clients will not see private notes. Transfer ticket to:  Project-Id-Version: Awesome Support: Private Notes
PO-Revision-Date: 2019-01-07 12:11-0200
Language-Team: Awesome Support <translate@getawesomesupport.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: .
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_ex:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
Last-Translator: daniel tygel <dtygel@eita.org.br>
Language: pt_BR
X-Poedit-SearchPath-0: .
X-Poedit-SearchPath-1: ..
 Adicionar nota interna Adicionar nota e transferir Adicionar uma nota interna a esta denúncia Esta nota só será vista por você e pelas outras pessoas da equipe do OPA. Os denunciantes não vêem as notas internas. Transferir a responsabilidade da denúncia para: 