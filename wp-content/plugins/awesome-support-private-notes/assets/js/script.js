(function ($) {
    "use strict";

    $(function () {
        // Show delete note alert
        $('.wpas-delete-note-btn').click(function (e) {
            if (confirm(wpas_pn.alertDelete)) {
                return true;
            } else {
                return false;
            }
        });


        // Open add user window
        $('#wp-admin-bar-wpas-private-note a').on('click', function (e) {

            e.preventDefault();
    
            var lightbox_id = $(this).attr('href');
    
            $(lightbox_id).fadeIn();
    
        });
    
    
        /**
         * Close lightbox
         */
        $(document).on('click', '.wpas-lightbox-close', function (e) {
            e.preventDefault();
            closeLightbox();
        });
    
    
        /**
         * Close print window on ESC
         */
        $(document).on('keyup', function (e) {
            if (e.keyCode == 27) {
                closeLightbox();
            }
        });


        /**
         * Close lightbox and delete temp directory for this ticket
         */
        function closeLightbox() {

            var ticket_id = (  $('#post_ID').length ) ? $('#post_ID').val() : $(this).data('ticket-id');

            $.ajax({
                type: 'POST',
                url: ajaxurl,
                async: false,
                data: {
                    action: 'wpas_delete_temp_directory',
                    ticket_id: ticket_id,
                }
            });

            $('.wpas-lightbox').fadeOut();

        }

    });

}(jQuery));

