<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

class WPAS_PN_File_Upload  extends WPAS_File_Upload {

	/**
	 * Instance of this class.
	 *
	 * @var      object
	 */
	protected static $instance = null;

	public    $post_id   = null;
	protected $parent_id = null;
	protected $index     = 'pn_files';

	/**
	 * Store the potential error messages.
	 */
	protected $error_message;

	public function __construct() {

		if ( is_admin() ) {
			
			add_action( 'wpas_private_note_field_after',		array( $this, 'upload_field') );
			add_action( 'wpas_private_note_content_after',		array( $this, 'show_attachments' ) );
			add_action( 'wpas_add_private_note_after',			array( $this, 'new_private_note_attachments' ), 11, 2 );
			add_action( 'wp_ajax_wpas_delete_attachment',		array( $this, 'ajax_delete_attachment' ), 9 );
			add_filter( 'wpas_filter_out_media_attachment_post_types', array( $this, 'filter_out_media_private_attachments' ) );
		}
		
	}
	
	
	/**
	 * Show private note attachments
	 * 
	 * @param int $post_id
	 */
	function show_attachments( $post_id ) {
		
		add_filter( 'wpas_can_delete_attachments',		'wpas_pn_user_can_delete_private_attachment', 99, 1 );
		
		parent::show_attachments( $post_id );
		
		remove_filter( 'wpas_can_delete_attachments',	'wpas_pn_user_can_delete_private_attachment', 99, 1 );
		
	}
	
	function filter_out_media_private_attachments( $post_types ) {
		
		$post_types[] = 'ticket_note';
		
		return $post_types;
	}
	
	
	/**
	 * Handle new private attachments
	 * 
	 * @param int $note_id
	 * @param array $data
	 */
	public function new_private_note_attachments( $note_id, $data ) {
		
		if( wpas_user_can_upload_private_attachments() && $this->can_attach_files() ) {
			if( boolval( wpas_get_option( 'private_notes_ajax_uploader', false ) ) ) {
				$this->process_ajax_upload( $data[ 'post_parent' ], $note_id, $data );
			} else {
				$this->post_id   = intval( $note_id );
				$this->parent_id = intval( $data['post_parent'] );
				$this->process_upload();
			}
		}
	}

	/**
	 * Delete single private attachment via ajax
	 * 
	 * @return void
	 */
	public function ajax_delete_attachment() {
		
		$note_id		= filter_input( INPUT_POST, 'parent_id', FILTER_SANITIZE_NUMBER_INT );
		$attachment_id  = filter_input( INPUT_POST, 'att_id', FILTER_SANITIZE_NUMBER_INT );
		
		$user = wp_get_current_user();
		
		
		if( !$note_id ) {
			return;
		}
		
		
		$note = get_post( $note_id );
		if( 'ticket_note' !== $note->post_type ) {
			return;
		}
		
		
		$can_delete = wpas_pn_user_can_delete_private_attachment();
		$deleted = false;
		
		if( $user && $attachment_id && $can_delete ) {
			
			wp_delete_attachment( $attachment_id, true );
			$deleted = true;
		}
			
		
		if( $deleted ) {
			wp_send_json_success( array( 'msg' => __( 'Attachment deleted.', 'as-private-notes' ) ) );
		} else {
			wp_send_json_error();
		}
		
		die();
	}
	
	
	/**
	 * Return an instance of this class.
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		
		
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	
	/**
	 * Add upload private attachments field
	 */
	public function upload_field() {
		
		if( wpas_user_can_upload_private_attachments() ) {
			
			add_filter( 'wpas_ticket_attachments_field_args', array( $this, 'attachment_field_args' ), 1 );
			
			parent::upload_field();
			
			remove_filter( 'wpas_ticket_attachments_field_args', array( $this, 'attachment_field_args' ), 1 );
		}
	}
	
	/**
	 * Set params for private attachments field
	 * 
	 * @param array $args
	 * 
	 * @return array
	 */
	function attachment_field_args( $args ) {
		
		$args['args']['use_ajax_uploader']	= boolval( wpas_get_option( 'private_notes_ajax_uploader', false ) );
		$args['args']['enable_paste']		= ( boolval( wpas_get_option( 'ajax_upload_paste_image', false ) ) );
		
		return $args;
	}
}
