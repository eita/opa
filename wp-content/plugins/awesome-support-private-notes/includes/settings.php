<?php

add_filter( 'wpas_plugin_settings', 'wpas_pn_all_settings', 10, 1 );

/**
 * 
 * Add private note settings
 * 
 * @param array $def
 * 
 * @return array
 */

function wpas_pn_all_settings( $def ) {
	
	$settings = array(
		'private_notes' => array(
			'name'    => __( 'Private Notes', 'as-private-notes' ),
			'options' => array(

				array(
					'name'    => __( 'Email Notification : Private Note Created', 'as-private-notes' ),
					'desc'    => __( 'Sent to other agents on the ticket as well as anyone else designated to receive private notes notifications (such as certain other users that can be added to the ticket by the POWERPACK add-on.)', 'as-private-notes' ),
					'type'    => 'heading'
				),

				array(
						'name'    => __( 'Enable', 'as-private-notes' ),
						'id'      => 'enable_private_note_created',
						'type'    => 'checkbox',
						'default' => true,
						'desc'    => __( 'Send email to agents when their a private note is created', 'as-private-notes' )
				),

				array(
						'name'    => __( 'All Agents', 'as-private-notes' ),
						'id'      => 'enable_pn_created_all_agents',
						'type'    => 'checkbox',
						'default' => true,
						'desc'    => __( 'Send this alert to agents who responded to the ticket but who are not formally assigned to it.', 'as-private-notes' )
				),					

				array(
						'name'    => __( 'Subject', 'as-private-notes' ),
						'id'      => 'private_note_created_email__subject',
						'type'    => 'text',
						'default' => __( 'A new private note created', 'as-private-notes' )
				),

				array(
						'name'    => __( 'Content', 'as-private-notes' ),
						'id'      => 'private_note_created_email__content',
						'type'    => 'editor',
						'default' => '',
						'desc'    => __( 'Email Content', 'as-private-notes' )
				),

				array(
					'name'    => __( 'Use AJAX Uploader', 'as-private-notes' ),
					'id'      => 'private_notes_ajax_uploader',
					'type'    => 'checkbox',
					'desc'    => __( 'Use the drag-and-drop uploader for uploading files', 'as-private-notes' ),
					'default' => false,
				),					

				array(
					'name'    => __( 'Roles That Can Delete Private Attachments', 'as-private-notes' ),
					'id'      => 'roles_delete_private_attachments',
					'type'    => 'text',
					'desc'    => __( 'Enter a comma separated list of roles that should be able to delete private attachments. Roles should be the internal WordPress role id such as wpas_agent and are case sensitive. There should be no spaces between the commas and role names when entering multiple roles.', 'as-private-notes' ),
					'default' => '',
				),
				array(
					'name'    => __( 'Allow Agents To Delete Their Own Private Notes', 'as-private-notes' ),
					'id'      => 'agent_delete_own_private_note',
					'type'    => 'checkbox',
					'desc'    => __( 'Would you like agents to be able to delete their own private notes? (FYI: We really do not recommend allowing deletes but you do have the option if you want it!)', 'as-private-notes' ),
					'default' => false
				),
				array(
					'name'    => __( 'Roles That Can Delete All Private Notes', 'as-private-notes' ),
					'id'      => 'roles_delete_all_private_notes',
					'type'    => 'text',
					'desc'    => __( 'Enter a comma separated list of roles that should be able to delete any agent and user private note. Roles should be the internal WordPress role id such as wpas_agent and are case sensitive. There should be no spaces between the commas and role names when entering multiple roles.', 'as-private-notes' ),
					'default' => '',
				),

				array(
					'name'    => __( 'Permanently Delete Private Notes', 'as-private-notes' ),
					'id'      => 'permanently_delete_private_notes',
					'type'    => 'checkbox',
					'desc'    => __( 'Check this to allow private notes to be permanently deleted. If you do not check this option, deleted items will end up in the WordPress trash bin which will allow you to see the deletion in-line.  They can be permanently deleted later with most third-party clean-up plugins.', 'as-private-notes' ),
					'default' => false
				),

			)
		)
	);

	return array_merge( $def, $settings );
	
}