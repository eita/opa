<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Check if user can upload private attachments
 * 
 * @param int $user_id
 * 
 * @return boolean
 */
function wpas_user_can_upload_private_attachments( $user_id = null ) {
	
	if( null === $user_id ) {
		
		$user_id = get_current_user_id();
	}
	
	$can = false;
	
	if( wpas_is_agent( $user_id ) ) {
		$can = true;
	}
	
	
	return $can;
}


/**
 * Check if user can delete private attachments
 * 
 * @param boolean $can_delete
 * 
 * @return boolean
 */
function wpas_pn_user_can_delete_private_attachment( $can_delete = false ) {
	
	$can_delete = false;
	
	if ( wpas_is_asadmin() || true === wpas_current_role_in_list( wpas_get_option( 'roles_delete_private_attachments', '' ) ) ) {
		$can_delete = true;
	}
	
	return $can_delete;
}

/**
 * Check if user can delete private note
 * 
 * @param int $note_id
 * 
 * @return boolean
 */
function wpas_pn_user_can_delete_note( $note_id ) {
	
	$note = get_post( $note_id );
	
	$can_delete = false;
	
	if( true === boolval( wpas_get_option( 'agent_delete_own_private_note', false ) ) && get_current_user_id() == $note->post_author ) {
		$can_delete = true;
	}
	
	if( true === wpas_is_asadmin()  ) {
		$can_delete = true;
	}
	
	if( true === wpas_current_role_in_list( wpas_get_option( 'roles_delete_all_private_notes', '' ) ) ) {
		$can_delete = true;
	}
		
	return $can_delete;
}

add_filter( 'attachments_list_for_auto_delete', 'wpas_pn_auto_delete_private_attachments_list' ,11, 2 );
/**
 * Add private attachments in auto delete list
 * 
 * @param array $attachments
 * @param int $ticket_id
 * 
 * @return array
 */
function wpas_pn_auto_delete_private_attachments_list( $attachments, $ticket_id ) {
	
	$args = array(
		'post_parent'            => $ticket_id,
		'post_type'              => 'ticket_note',
		'post_status'            => 'any',
		'posts_per_page'         => - 1
	);
	
	$notes_query = new WP_Query( $args );
	$ticket_notes = $notes_query->posts;
	
	foreach( $ticket_notes as $note ) {
		$attachments = array_merge( $attachments, get_attached_media( '', $note->ID ) );
	}
	
	return $attachments;
}

add_action( 'wpas_do_admin_trash_private_note', 'wpas_admin_action_trash_private_note' );
/**
 * Trash single private note
 * 
 * @param array $data
 * 
 * @return boolean
 */
function wpas_admin_action_trash_private_note( $data ) {
		
	if ( ! is_admin() || ! isset( $data['note_id'] ) ) {
		return;
	}

	// Extract the noteid/postid from the array passed into this function
	$note_id = (int) $data['note_id'];

	// If user not allowed to delete note just bail out
	if( !wpas_pn_user_can_delete_note( $note_id ) ) {
		return false;
	}

	$ticket_id = ''; // will populate this below...
	$trashed_post = null ; // will populate this below
	
	/* Get the contents of the post being trashed */
	$trashed_contents = '';		
	if ( $note_id > 0 ) {

		$trashed_post = get_post( $note_id );

		if ( ! is_null( $note_id ) ) {
			$trashed_contents = $trashed_post->post_content ;
			$ticket_id = $trashed_post->post_parent;
		} else {
			return false ;
		}
	}

	/* Delete the attachments first in case the note is permanently deleted */
	wpas_delete_post_attachments( $note_id );
	
	/* Now trash/delete the post */
	if ( boolval( wpas_get_option( 'permanently_delete_private_notes', false ) ) ) {
		wp_delete_post( $note_id, true ); // Permanentaly delete		
	} else {
		wp_trash_post( $note_id );  // Move to trash		
	}
	
	/* Add a flag to the TICKET that shows one of its replies was deleted */
	update_post_meta( $ticket_id, 'wpas_private_note_was_deleted', '1' ) ;
	
	/* Fire the after-delete action hook */
	do_action( 'wpas_admin_private_note_trashed', $note_id, $data, $trashed_post );	
	
	// Read-only redirect
	$redirect_to = add_query_arg( array(
		'action'       => 'edit',
		'post'         => $data['post'],
	), admin_url( 'post.php' ) );

	wp_redirect( wp_sanitize_redirect( "$redirect_to#wpas-post-$note_id" ) );
	exit;
}

add_action( 'wpas_backend_ticket_content_after', 'wpas_show_note_deleted_msg', 10, 2 );
/**
 * Show whether a ticket note has been deleted.
 *
 * Because the note might be permanently deleted, 
 * we have to show the message on the opening ticket post.
 *
 * Action hook: wpas_backend_ticket_content_after
 *
 * @since 3.0.0
 *
 * @param string $ticket_id - id of ticket being processed.
 * @param array  $ticket    - post object of ticket being processed.
 *
 * @return void
 */
function wpas_show_note_deleted_msg( $ticket_id, $ticket ) {

	$post = get_post_meta( $ticket_id, 'wpas_private_note_was_deleted' );

	if ( (int) $post > 0 ) {
		echo '<br />' . '<div class="wpas_footer_note">' . __( '* Private notes have been deleted from this ticket. Depending on your settings, logs of these deletions might not be available.', 'as-private-notes' ) . '</div>';
	}

}

add_action( 'wpas_system_tools_table_after',  'wpas_add_delete_private_notes_tool',   14, 0 );
/**
 * Add new tool to delete all private notes
 */
function wpas_add_delete_private_notes_tool() {
	?>

	<tr>
		<td class="row-title"><label for="tablecell"><?php _e( 'Delete All Private Notes', 'as-private-notes' ); ?></label></td>
		<td>
			<a href="<?php echo wpas_tool_link( 'delete_all_private_notes' ); ?>"
			   class="button-secondary"><?php _e( 'Delete', 'as-private-notes' ); ?></a>
			<span
				class="wpas-system-tools-desc"><?php _e( 'Delete all Private Notes from all tickets.', 'as-private-notes' ); ?></span>
		</td>
	</tr>
	<?php
}

add_action( 'execute_additional_tools', 'wpas_pn_delete_all_private_notes', 11 );
/**
 * Handle request to delete all private notes
 * 
 * @param string $tool
 * 
 * @return void
 */
function wpas_pn_delete_all_private_notes( $tool ) {
	
	if( 'delete_all_private_notes' !== $tool ) {
		return;
	}
	
	$posts = get_posts( array(
		'post_type'   => 'ticket_note',
		'post_status' => array( 'publish' ),
		'numberposts' => '-1'
	) );
	
	foreach( $posts as $post ) {
		wp_trash_post( $post->ID, false );
	}

}
