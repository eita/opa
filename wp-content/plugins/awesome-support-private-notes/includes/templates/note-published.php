<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
?>

<td class="col1">
	<div class="wpas-ticket-note-heading"></div>
	<?php echo get_avatar( $post->post_author, '64', get_option( 'avatar_default' ) ); ?>
</td>
<td class="col2">
	<div class="wpas-ticket-note-heading"><span class="dashicons dashicons-lock"></span> <?php _e( 'This is a private note', 'as-private-notes' ); ?></div>
	<div class="wpas-reply-meta">
		<div class="wpas-reply-user">
			<strong class="wpas-profilename"><?php echo $user_name; ?></strong> <span class="wpas-profilerole">(<?php echo wpas_get_user_nice_role( $user_data->roles ); ?>)</span>
		</div>
		<div class="wpas-reply-time">
			<time class="wpas-timestamp" datetime="<?php echo get_the_date( 'Y-m-d\TH:i:s' ) . wpas_get_offset_html5(); ?>"><span class="wpas-human-date"><?php echo date( get_option( 'date_format' ), strtotime( $post->post_date ) ); ?> | </span><?php printf( __( '%s ago', 'as-private-notes' ), $date ); ?></time>
		</div>
	</div>

	<?php
	// Delete button link
	if( wpas_pn_user_can_delete_note( $post->ID ) ) {
		$delete_link = wpas_do_url( admin_url( 'post.php' ), 'admin_trash_private_note', array( 'post' => $post->post_parent, 'action' => 'edit', 'note_id' => $post->ID ) );
		printf( '<a href="%s" class="wpas-delete-note-btn">%s</a>', $delete_link, __( 'Delete', 'as-private-notes' ) );
	}
	
	/* Filter the content before we display it */
	$content = apply_filters( 'the_content', $post->post_content );

	/* The content displayed to agents */
	echo '<div class="wpas-reply-content" id="wpas-reply-' . $post->ID . '">';

	/**
	 * wpas_backend_reply_content_before hook
	 *
	 * @since  3.0.0
	 */
	do_action( 'wpas_private_note_content_before', $post->ID );

	echo $content;

	/**
	 * wpas_backend_reply_content_after hook
	 *
	 * @since  3.0.0
	 */
	do_action( 'wpas_private_note_content_after', $post->ID );

	echo '</div>';
	?>
</td>