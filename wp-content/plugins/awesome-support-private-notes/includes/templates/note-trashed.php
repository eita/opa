<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

?>
<td colspan="3">
	<?php printf( __( 'This note has been deleted by %s <em class="wpas-time">%s ago.</em>', 'as-private-notes' ), "<strong>$user_name</strong>", human_time_diff( strtotime( $post->post_modified ), current_time( 'timestamp' ) ) ); ?>
</td>
