<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


class WPAS_PN_Email_Notification extends WPAS_Email_Notification {
	
	
	public function __construct( $post_id ) {

		/* Make sure the given post belongs to our plugin. */
		if ( 'ticket_note' !== get_post_type( $post_id ) ) {
			return new WP_Error( 'incorrect_post_type', __( 'The post ID provided does not match any of the plugin post types', 'as-private-notes' ) );
		}

		/* Set the e-mail content type to HTML */
		add_filter( 'wp_mail_content_type', array( $this, 'set_html_mime_type' ) );

		/* Set the post ID */
		$this->post_id = $post_id;
		
		$this->ticket_id = $this->get_ticket_id();

	}
	
	/**
	 * Get the post object for the unassigned ticket.
	 *
	 * @return boolean|object The ticket object if there is a reply, false otherwise
	 */
	public function get_ticket() {

		if ( isset( $this->ticket ) ) {
			return $this->ticket;
		}
		
		$this->ticket = get_post( $this->get_ticket_id() );

		return $this->ticket;

	}
	
	/**
	 * Get ticket id
	 * 
	 * @return int
	 */
	public function get_ticket_id() {
		
		if( $this->ticket_id ) {
			return $this->ticket_id;
		}
		
		$note = $this->get_reply();
		
		return  $note->post_parent;
		
	}
	
	/**
	 * Get the post object for the reply.
	 *
	 * @return boolean|object The reply object if there is a reply, false otherwise
	 */
	public function get_reply() {

		if ( isset( $this->reply ) ) {
			return $this->reply;
		}

		$this->reply = get_post( $this->post_id );

		return $this->reply;

	}
	
}

add_filter( 'wpas_email_notifications_email', 'wpas_pn_set_email_recipients', 10, 3 );
/**
 * Set recipients for private note created e-mail notification
 * 
 * @param array $args
 * @param string $case
 * @param int $ticket_id
 * 
 * @return array
 */
function wpas_pn_set_email_recipients( $args, $case, $ticket_id ) {

	if( 'private_note_created' === $case ) {
		
		/* Get a list of agents associated with the ticket */
		if ( boolval( wpas_get_option('enable_pn_created_all_agents', true ) ) ) {
			$agents = wpas_id_to_user_object( wpas_get_all_agents_on_ticket( $ticket_id ) ) ;
		} else {
			$agents = wpas_get_ticket_agents( $ticket_id, array( get_current_user_id() ) );
		}
		
		/* Convert the agent list to an array of emails... */
		$emails = $args['recipient_email'] ? $args['recipient_email'] : array();
		
		foreach( $agents as $agent ) {
			if ( get_current_user_id() <> $agent->ID ) {
				$emails[] = array( 'user_id' => $agent->ID, 'email' => $agent->user_email );
			}
		}

		// Get recipients from productivity add-on and check if notification for private notes is active for each user contact or email address
		if( class_exists( 'WPAS_PF_Ticket_User_Contact') ) {
		
			$meta_keys = array( 'user_contacts', 'notification_emails' );

			foreach( $meta_keys as $meta_key ) {
				$items = maybe_unserialize( get_post_meta( $ticket_id , $meta_key, true ) );

				$items = !is_array( $items ) ? array() : $items;
				
				
				foreach ( $items as $item ) {

					if( isset( $item['pn_active'] ) && $item['pn_active'] ) {

						if( isset( $item['email'] ) ) {
							$emails[] = $item['email'];
						} elseif ( isset ( $item['user_id'] ) ) { 
							
							$item_user = get_user_by( 'id',  $item['user_id'] );
							if( $item_user ) {
								$emails[] = array( 'user_id' => $item_user->ID, 'email' => $item_user->user_email );
							}
						}
						
					}
				}
			}
		
		}
		
		$args['recipient_email'] = $emails;
		
	}
	
	return $args;

}

add_filter( 'wpas_email_notifications_pre_fetch_content', 'wpas_pn_set_email_content', 10, 3 );
/**
 * Get private note created e-mail notification content
 *
 * @param string $value     Notification content
 * @param int    $ticket_id ID of the ticket being processed
 * @param string $case      Case of the notification being sent
 *
 * @return string
 */
function wpas_pn_set_email_content( $value, $ticket_id, $case ) {

	if ( 'private_note_created' === $case ) {
		$value = wpas_get_option( "{$case}_email__content" );
	}

	return $value;

}

add_filter( 'wpas_email_notifications_pre_fetch_subject', 'wpas_pn_set_email_subject', 9, 3 );
/**
 * Get private note created e-mail notification subject
 *
 * @param string $value     Notification subject
 * @param int    $ticket_id ID of the ticket being processed
 * @param string $case      Case of the notification being sent
 *
 * @return string
 */
function wpas_pn_set_email_subject( $value, $ticket_id, $case ) {
	
	if ( 'private_note_created' === $case ) {
		$value = wpas_get_option( "{$case}_email__subject" );
	}
	
	return $value;
}

add_filter( 'wpas_email_notifications_cases', 'wpas_pn_add_private_note_created_notification_case' );
/**
 * Register private note created case
 
 * @param array $cases Existing notification cases
 *
 * @return array
 */
function wpas_pn_add_private_note_created_notification_case( $cases ) {
	
	$cases[] = 'private_note_created';
	
	return $cases;
}

add_filter( 'wpas_email_notifications_cases_active_option', 'wpas_pn_private_note_created_active_option' );


/**
 * Set private note created notification active option name
 *
 * @param array $cases Array of cases with their "enable" option name
 *
 * @return array
 */
function wpas_pn_private_note_created_active_option( $cases ) { 
	$cases['private_note_created'] = 'enable_private_note_created';
	
	return $cases;
}

add_action( 'wpas_add_private_note_after', 'wpas_pn_notify_private_note_created', 99, 2 );

/**
 * Send private note created notification
 * 
 * @param int $note_id
 * @param array $args
 * 
 * @return WP_Error|Boolean
 */
function wpas_pn_notify_private_note_created( $note_id, $args ) {
	
	$subject = wpas_get_option( "private_note_created_email__subject" );
	$content = wpas_get_option( "private_note_created_email__content" );
	
	if( empty( trim( $subject ) ) && empty( trim( $content ) ) ) {
		return;
	}
	
	if( class_exists( 'WPAS_PF_Ticket_User_Contact') ) {
		$pf_user_contacts_class =  WPAS_PF_Ticket_User_Contact::get_instance();
		remove_filter( 'wpas_email_notifications_email', array( $pf_user_contacts_class, 'set_user_contact_emails' ), 11 );
	}
	
	if( class_exists( 'WPAS_PF_Ticket_Notification_Email' ) ) {
		$pf_ticket_email_class = WPAS_PF_Ticket_Notification_Email::get_instance();
		remove_filter( 'wpas_email_notifications_email', array( $pf_ticket_email_class, 'set_notification_emails'), 11 );
	}
	
	$notification = new WPAS_PN_Email_Notification( $note_id );
	
	if ( is_wp_error( $notification ) ) {
		return $notification;
	}
	
	$result = $notification->notify( 'private_note_created' );
	
	if( class_exists( 'WPAS_PF_Ticket_User_Contact') ) {
		$pf_user_contacts_class =  WPAS_PF_Ticket_User_Contact::get_instance();
		add_filter( 'wpas_email_notifications_email', array( $pf_user_contacts_class, 'set_user_contact_emails' ), 11, 3 );
	}
	
	if( class_exists( 'WPAS_PF_Ticket_Notification_Email' ) ) {
		$pf_ticket_email_class = WPAS_PF_Ticket_Notification_Email::get_instance();
		add_filter( 'wpas_email_notifications_email', array( $pf_ticket_email_class, 'set_notification_emails'), 11, 3 );
	}
	
	return $result;
}


add_action( 'wpas_pf_add_ticket_user_contact_checkboxes_after',			'wpas_pn_add_notification_checkbox', 11, 2 );
add_action( 'wpas_pf_add_ticket_notification_email_checkboxes_after',	'wpas_pn_add_notification_checkbox', 11, 2 );
/**
 * Add new checkbox to enable or disable private note email notifications for user contacts and email addresses
 * 
 * @param array $item
 * 
 * @param string $type
 */
function wpas_pn_add_notification_checkbox( $item, $type ) {
	 
	 $active = '';
	 
	 if( $item ) {
		$active = isset( $item['data']['pn_active'] ) && $item['data']['pn_active'] ? true : false;
	 }
	 
	 ?>
	<div>
		<label><input type="checkbox"<?php echo ( ( $active ) ? ' checked="checked"' : '' ); ?> data-name="pn_active" value="1" /> <?php _e( 'Receive Private Note Notification Emails' ); ?></label>
	</div>
	<?php
	 
}


add_action ( 'wpas_pf_ticket_notification_email_item_tags_after', 'wpas_pn_add_notification_active_tag' );
add_action ( 'wpas_pf_ticket_user_contact_item_tags_after',       'wpas_pn_add_notification_active_tag' );
/**
 * Add Private Note tag in powerpack add-on under user contacts and email addresses tabs
 * 
 * @param array $item
 */
function wpas_pn_add_notification_active_tag( $item ) {
	 $active = '';
	 
	 if( $item ) {
		$active = isset( $item['pn_active'] ) && $item['pn_active'] ? true : false;
	 }
	 
	 if( $active ) {
		 printf( '<span class="wpas-label">%s</span>', __( 'Private Note', 'as-private-notes' ) );
	 }
}

add_filter( 'wpas_pf_ticket_notification_email_new_item',		'wpas_pn_save_private_notes_notification_active' );
add_filter( 'wpas_pf_ticket_notification_email_update_item',	'wpas_pn_save_private_notes_notification_active' );
add_filter( 'wpas_pf_ticket_user_contact_new_item',				'wpas_pn_save_private_notes_notification_active' );
add_filter( 'wpas_pf_ticket_user_contact_update_item',			'wpas_pn_save_private_notes_notification_active' );
/**
 * Save enable notification status for user contact and email addresses
 * 
 * @param array $item
 * 
 * @return array
 */
function wpas_pn_save_private_notes_notification_active( $item ) {
	
	$active  = filter_input( INPUT_POST, 'pn_active' );
	$item['pn_active'] = $active ? true : false;
	
	return $item;
}

add_filter( 'wpas_get_users_on_ticket_post_types', 'wpas_pn_set_users_on_ticket_post_type' );
/**
 * Make sure the core knows about the ticket_note post type when evaluating users that have contributed to a ticket
 * 
 * @param array $post_types
 * 
 * @return array
 */
function wpas_pn_set_users_on_ticket_post_type( $post_types ) {
	
	$post_types[] = 'ticket_note';
	
	return $post_types ;
}
