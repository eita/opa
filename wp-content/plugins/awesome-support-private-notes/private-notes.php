<?php
/**
 * @package   Awesome Support Private Notes
 * @author    Awesome Support <contact@getawesomesupport.com>
 * @license   GPL-2.0+
 * @link      https://getawesomesupport.com
 * @copyright 2014-2018 Awesome Support
 *
 * @wordpress-plugin
 * Plugin Name:       Awesome Support: Private Notes
 * Plugin URI:        http://getawesomesupport.com/addons/private-notes/
 * Description:       This add-on for Awesome Support enables you to add notes to a ticket that clients won't see. You can use these notes as reminders or to give other agents insights into the ticket issue.
 * Version:           3.1.1
 * Author:            The Awesome Support Team
 * Author URI:        https://getawesomesupport.com
 * Text Domain:       as-private-notes
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


/* 
 * Include extension base class and make sure that the Awesome Support plugin is enabled.
 *
 * To do this, we perform checks in the following order:
 *  1. Check if the WPAS_Extension_Base class is available.  
 *  2. If not, check if the variable WPAS_ROOT is defined and the file is available in that path.
 *  3. If not, check if the variable WPAS_AS_FOLDER is defined and the file is available in that path.
 * 
 * If the file does not exist, we throw up an error message.
 */
if ( !class_exists( 'WPAS_Extension_Base' ) ) {
	
	$wpas_dir = defined( 'WPAS_ROOT' )  ? WPAS_ROOT : ( defined( 'WPAS_AS_FOLDER' ) ? WPAS_AS_FOLDER : 'awesome-support' );
	$wpas_eb_file = trailingslashit( WP_PLUGIN_DIR . '/' . $wpas_dir ) . 'includes/class-extension-base.php';
	
	if( file_exists( $wpas_eb_file ) ) {
		require_once ( $wpas_eb_file );
	} else {
		add_action( 'admin_notices', function() {
		?>	
		
		<div class="error">
			<p>
				<?php printf( __( 'The LATEST version of Awesome Support needs to be installed in order to activate the Private Notes add-on for Awesome Support. Please <a href="%s" target="_blank">install Awesome Support</a> before continuing.', 'as-private-notes' ), esc_url( 'https://getawesomesupport.com' ) ); ?>
			</p>
		</div>
			
		<?php	
			
		});
		
		return;
	}
}

/*----------------------------------------------------------------------------*
 * Instantiate the plugin
 *----------------------------------------------------------------------------*/

/**
 * Register the activation hook
 */
register_activation_hook(   __FILE__, array( 'WPAS_Private_Note', 'activate'   ) );

add_action( 'plugins_loaded', array( 'WPAS_Private_Note', 'get_instance' ), 10, 0 );


class WPAS_Private_Note extends WPAS_Extension_Base {

	/**
	 * Instance of this loader class.
	 *
	 * @since    0.1.0
	 * @var      object
	 */
	protected static $instance = null;
	
	/**
	 * addon construct method
	 */
	public function __construct() {
		
		$this->setVersionRequired( '5.2.0' );		// Set required version of core
		$this->setPhpVersionRequired( '5.6' );		// Set required version of php
		$this->setSlug( 'private-notes' );			// Set addon slug
		$this->setUid( 'PRN' );						// Set short unique id
		$this->setTextDomain( 'as-private-notes' ); // Set text domain for translation
		$this->setVersion( '3.1.1' );				// Set addon version
		$this->setItemId( 59 );						// Set addon item id
		
		parent::__construct();
	}
	
	
	/**
	 * Load required files
	 */
	public function load() {

		add_action( 'after_setup_theme',      array( $this, 'post_type' ),          10, 0 );
		add_filter( 'wpas_replies_post_type', array( $this, 'add_note_post_type' ), 10, 1 );
		add_filter( 'wpas_addons_licenses',   array( $this, 'addon_license' ),      10, 1 );
		add_action( 'wpas_backend_replies_inside_row_after', array( $this, 'note_row_content' ), 10, 1 );
		
		add_action( 'ticket_listing_after_search_controls', array( $this, 'add_search_checkbox' ), 11, 0 );
		add_filter( 'ticket_listing_search_joins',			array( $this, 'ticket_listing_search_joins' ) );
		add_filter( 'ticket_listing_search_clauses',		array( $this, 'ticket_listing_search_clauses' ), 11, 2 );
		
		require_once WPAS_PRN_PATH . 'includes/functions.php';
		require_once WPAS_PRN_PATH . 'includes/settings.php';
		require_once WPAS_PRN_PATH . 'includes/class-file-uploader.php';
		require_once WPAS_PRN_PATH . 'includes/class-email.php';
		
		WPAS_PN_File_Upload::get_instance();

		// Load the plugin translation.
		add_action( 'plugins_loaded', array( $this, 'load_plugin_textdomain' ), 15 );
		
		if ( is_admin() ) {
			if ( isset( $_GET['post'] ) && 'ticket' === get_post_type( intval( $_GET['post'] ) ) ) {
				add_action( 'admin_bar_menu',         array( $this, 'add_new_note_link' ), 9999 );
				add_action( 'admin_init',             array( $this, 'save_note' ),         10, 0 );
				add_action( 'in_admin_footer',        array( $this, 'modal_box' ),         10, 0 );
				add_action( 'admin_enqueue_scripts',  array( $this, 'enqueue_styles' ),    10, 0 );
			}
		}
	}

	
	/**
	 * Add search clause for ticket listing search
	 * 
	 * @global object $wpdb
	 * 
	 * @param array $search_clauses
	 * @param object $query
	 * 
	 * @return array
	 */
	public function ticket_listing_search_clauses( $search_clauses, $query ) {
		global $wpdb;
		
		$search_by = filter_input( INPUT_GET, 'search_by', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY );
		
		if( is_array( $search_by ) && in_array( 'private_notes', $search_by ) ) {
			
			$term = $query->get('s');
			
			$like = '%' . $wpdb->esc_like( $term ) . '%';
			
			$search_clauses[] = $wpdb->prepare( '(wptnp.post_excerpt LIKE %s) OR (wptnp.post_content LIKE %s)', $like, $like );
		}
		
		return $search_clauses;
	}
	
	
	/**
	 * Add joins for ticket listing search query
	 * 
	 * @global object $wpdb
	 * 
	 * @param array $joins
	 * 
	 * @return array
	 */
	public function ticket_listing_search_joins( $joins ) {
		global $wpdb;
		
		$search_by = filter_input( INPUT_GET, 'search_by', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY );
		
		if( is_array( $search_by ) && in_array( 'private_notes', $search_by ) ) {
			$joins[] = " LEFT JOIN {$wpdb->posts} wptnp ON ({$wpdb->posts}.ID = wptnp.post_parent) AND wptnp.post_type='ticket_note'";
		}
		
		return $joins;
	}
	
	public function add_search_checkbox() {
		
		
		
		$search_by = filter_input( INPUT_GET, 'search_by', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY );
		$private_notes_checked = $search_by && is_array( $search_by ) && in_array( 'private_notes', $search_by ) ? true : false;
		
		?>
		
		<label><input type="checkbox" name="search_by[]" value="private_notes" <?php checked( true, $private_notes_checked ); ?> /> <?php _e( 'Private Notes', 'as-private-notes' ); ?></label>
		
		<?php
	}
	/**
	 * Load addon styles.
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function enqueue_styles() {
		wp_enqueue_style( 'wpaspr-admin-style', $this->getAddonUrl() . 'assets/css/admin.css', array(  ), $this->getVersion(), 'all' );		
		$localize_script = array( 'alertDelete' => __( 'Are you sure you want to delete this note?') );
		
		wp_enqueue_script( 'wpas-pn-script', $this->getAddonUrl() . 'assets/js/script.js', array( 'jquery' ), $this->getVersion(), 'all' );
		wp_localize_script( 'wpas-pn-script', 'wpas_pn', $localize_script );
		
	}


	public function post_type() {
		
	$labels = apply_filters( 'wpas_ticket_note_type_labels', array(
			'name'               => _x( 'Ticket Notes', 'post type general name', 'as-private-notes' ),
			'singular_name'      => _x( 'Ticket Note', 'post type singular name', 'as-private-notes' ),
			'menu_name'          => _x( 'Private Notes', 'admin menu', 'as-private-notes' ),
			'name_admin_bar'     => _x( 'Private Notes', 'add new on admin bar', 'as-private-notes' ),
			'add_new'            => _x( 'Add New', 'private notes', 'as-private-notes' ),
			'add_new_item'       => __( 'Add New Private Note', 'as-private-notes' ),
			'new_item'           => __( 'New Private Note', 'as-private-notes' ),
			'edit_item'          => __( 'Edit Private Note', 'as-private-notes' ),
			'view_item'          => __( 'View Private Note', 'as-private-notes' ),
			'all_items'          => __( 'All Private Notes', 'as-private-notes' ),
			'search_items'       => __( 'Search Private Notes', 'as-private-notes' ),
			'parent_item_colon'  => __( 'Parent Note:', 'as-private-notes' ),
			'not_found'          => __( 'No private notes found.', 'as-private-notes' ),
			'not_found_in_trash' => __( 'No private notes found in Trash.', 'as-private-notes' ),
	) );		

		/* Post type capabilities */
		$cap = array(
			'read'					 => 'view_ticket',
		'read_post'				 => 'view_ticket',
			'read_private_posts' 	 => 'view_private_ticket',
			'edit_post'				 => 'edit_ticket',
			'edit_posts'			 => 'edit_ticket',
			'edit_others_posts' 	 => 'edit_other_ticket',
			'edit_private_posts' 	 => 'edit_private_ticket',
			'edit_published_posts' 	 => 'edit_ticket',
			'publish_posts'			 => 'create_ticket',
			'delete_post'			 => 'delete_ticket',
			'delete_posts'			 => 'delete_ticket',
			'delete_private_posts' 	 => 'delete_private_ticket',
			'delete_published_posts' => 'delete_ticket',
			'delete_others_posts' 	 => 'delete_other_ticket'
		);

		/* Post type arguments */
		$args = array(
			'labels'              => $labels,		
			'public'              => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'show_ui'             => false,
			'show_in_menu'        => false,
			'query_var'           => true,
			'capability_type'     => 'edit_ticket',
			'capabilities'        => $cap,
			'has_archive'         => false,
			'hierarchical'        => false,
			'menu_position'       => null,
		);

		register_post_type( 'ticket_note', $args );

	}

	/**
	 * Add new note link.
	 *
	 * Adds a new link in admin bar to let agents
	 * create a new private note for the ticket
	 * currently being displayed.
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function add_new_note_link() { 

		global $wp_admin_bar, $current_user, $post, $pagenow;
		
		if ( current_user_can( 'edit_ticket' ) && 'post.php' === $pagenow && 'ticket' === $post->post_type ) {

			$wp_admin_bar->add_menu( array(
				'id' 	=> 'wpas-private-note',
				'title' => __( 'Add Note', 'as-private-notes' ),
				'href' 	=> '#wpas-private-notes-lightbox',
				'meta'  => array( 'title' => __( 'Add a private note to this ticket', 'as-private-notes' ) )
			) );

		}

	}

	/**
	 * Private note colorbox.
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function modal_box() {

		global $post; ?>

		<div id="wpas-private-notes-lightbox" class="wpas-lightbox">

			<a href="#" class="wpas-lightbox-close-icon wpas-lightbox-close" data-ticket-id="<?php echo $post->ID; ?>">x</a>

			<div class="wpas-lightbox-content">

				<h1><?php _e( 'Add a private note to this ticket', 'as-private-notes' ); ?></h1>
				<p><?php _e( 'This note will only be seen by you and other agents. Clients will not see private notes.', 'as-private-notes' ); ?></p>
				
				<form method="post" action="" id="wpas-new-note" enctype="multipart/form-data">
					<textarea name="wpas_note" id="wpas_note" rows="10" required autofocus></textarea>
					
					<?php do_action( 'wpas_private_note_field_after' ); ?>
					
					<input type="hidden" name="post_id" value="<?php echo $post->ID; ?>">
					<?php wp_nonce_field( 'add_note', '_wpas_note_nonce', false, true );

					/* get the roles */
					global $wp_roles;

					/* Prepare the empty users list */
					$users = array();

					/* Parse the roles */
					foreach( $wp_roles->roles as $role => $data ) {

						/* Check if current role can edit tickets */
						if( array_key_exists( 'edit_ticket', $data['capabilities'] ) ) {

							/* Get users with current role */
							$usrs = new WP_User_Query( array( 'role' => $role ) );

							/* Save users in global array */
							$users = array_merge( $users, $usrs->get_results() );
						}
					}
					?>

					<br />

					<div class="wpas-cf">

						<button type="submit" name="wpas-submit" class="button button-secondary wpas-pl" value="add"><?php _e( 'Add Note', 'as-private-notes' ); ?></button>
						<?php if( count( $users ) > 1 ): ?>

							<div class="wpas-pr">

								<label for="wpas-new-agent"><?php _e( 'Transfer ticket to: ', 'as-private-notes' ); ?></label>

								<select name="wpas_agent" id="wpas-new-agent">
									<?php
									foreach( $users as $usr => $data ) {

										if( get_current_user_id() !=  $data->ID ) {
											?><option value="<?php echo $data->ID; ?>"><?php echo $data->data->display_name; ?></option><?php
										}
									}
									?>
								</select>
								<button type="submit" name="wpas-submit" class="button button-secondary" value="add_transfer"><?php _e( 'Add Note &amp; Transfer', 'as-private-notes' ); ?></button>
							</div>

						<?php endif; ?>
						
					</div>

				</form>

			</div>
		</div>

	<?php }
	
	/**
	 * Add the note type.
	 *
	 * Adds the ticket_note post type to the list of post types
	 * to query when getting replies for a ticket.
	 *
	 * @since   0.1.0
	 * @param   array $types Post types to query
	 * @return  void
	 */
	public function add_note_post_type( $types ) {
		array_push( $types, 'ticket_note' );
		return $types;
	}

	/**
	 * Save private notes.
	 *
	 * @since  0.1.0
	 * @return mixed ID of the post on success, WP_Error on failure
	 */
	public function save_note() {

		if ( !isset( $_POST['wpas_note'] ) || empty( $_POST['wpas_note'] ) ) {
			return false;
		}

		if ( !isset( $_POST['_wpas_note_nonce'] ) || !wp_verify_nonce( $_POST['_wpas_note_nonce'], 'add_note' ) ) {
			return false;
		}
		
		global $current_user;

		$post_id = isset( $_GET['post'] ) ? intval( $_GET['post'] ) : '';
		$note    = wp_kses_post( $_POST['wpas_note'] );
		$args    = array(
			'post_content'   => $note,
			'post_type'      => 'ticket_note',
			'post_author'    => $current_user->ID,
			'post_parent'    => $post_id,
			'post_title'     => sprintf( __( 'Note to ticket %s', 'as-private-notes' ), "#$post_id" ),
			'post_status'    => 'publish',
			'ping_status'    => 'closed',
			'comment_status' => 'closed'
		);
		
		$insert = wp_insert_post( $args, true );
		
		if (!is_wp_error($insert)) {
			
			do_action( 'wpas_add_private_note_after', $insert, $args );
			
			wpas_email_notify($insert, 'ticket_note');
		}
		
		$transfer = isset( $_POST['wpas-submit'] ) && 'add_transfer' === $_POST['wpas-submit'] ? true : false;

		/* Transfer the ticket to another agent. */
		if ( isset( $_POST['wpas_agent'] ) && $transfer ) {

			$user = get_user_by( 'id', filter_input( INPUT_POST, 'wpas_agent', FILTER_SANITIZE_NUMBER_INT ) );

			if ( false !== $user ) {

				wpas_assign_ticket( $post_id, $user->ID );

				$args = array( 'post_type' => 'ticket' );

				if ( true === boolval( wpas_get_option( 'hide_closed' ) ) ) {
					$args['wpas_status'] = 'open';
				}

				wp_redirect( add_query_arg( $args, admin_url( 'edit.php' ) ) );
				exit;
			}
		}

		return $insert;

	}

	/**
	 * Diaplay the provate note.
	 *
	 * Adds the HTML markup to display the private note
	 * amongst all other post types.
	 *
	 * @since  0.1.0
	 * @param  object $post Post currently being displayed
	 * @return void
	 */
	public static function note_row_content( $post ) {
		
		if ( 'ticket_note' !== $post->post_type ) {
			return false;
		}
		
		$user_data = get_userdata( $post->post_author );
		$user_id   = $user_data->data->ID;
		$user_name = $user_data->data->display_name;
		$date      = human_time_diff( get_the_time( 'U', $post->ID ), current_time( 'timestamp' ) );
		
		if ( 'trash' != $post->post_status ) {
			require( WPAS_PRN_PATH . 'includes/templates/note-published.php' );
		} elseif ( 'trash' == $post->post_status ) {
			require( WPAS_PRN_PATH . 'includes/templates/note-trashed.php' );
		}
		
	}
	
}