# README #

This project is an extension designed to run on the AWESOME SUPPORT Wordpress Plugin Platform.  

### How do I get set up? ###

Installation is straightforward using the usual WordPress Plugins -> Add New procedure.

- Download the file from your receipt or from your dashboard(Awesome-Support-Private-Notes.zip).
- Within WordPress Dashboard, click `Plugins` -> `Add New`
- Click the `Upload` button and select the ZIP file you just downloaded.
- Click the `Install` button


### Change Log  ###
3.1.1
------
Fix: Ajax error when trying to close the lightbox and drag-and-drop attachments is turned off.

3.1
------
Tweak: Use a full-screen lightbox for notes instead of thickbox.
Fix: Using drag-and-drop uploads could sometimes leave temp folders around if the user did not save the note.

3.0
------
New: Allow attachments to be added to notes
New: Send alerts to other agents
New: Allow private notes to be deleted

2.1
------
New: Allow search of private notes in core Awesome Support (Requires Awesome Support 5.2.0 or later)
Tweak: Change the way an array of roles worked to account for how the User Role Editor plugin modifies it.  This now requires Awesome Support 5.2.0 or later.
Tweak: Internal change to call notification class when a note is added.

2.0.0
------
New: Uses the new extension base class that was introduced in Awesome Support version 5.0 

1.3.0
-----
Fix:  Explicitly label the ticket_note post type

0.1.2
-----
Updates: Clean up translation catalog, textdomains and add to POEDITOR.net
Fix: Force use of HTTPS when FORCE_SSL_ADMIN is set to true