# README #

This project is an extension designed to run on the AWESOME SUPPORT Wordpress Plugin Platform.  

### How do I get set up? ###

Installation is straightforward using the usual WordPress Plugins -> Add New procedure.

- Download the file from your receipt or from your dashboard(Awesome-Support-Faq.zip).
- Within WordPress Dashboard, click `Plugins` -> `Add New`
- Click the `Upload` button and select the ZIP file you just downloaded.
- Click the `Install` button


### Change Log  ###
3.1.0
-----
New: Requires Awesome Support 5.8.0 or later
Tweak: For icon changes related to AS 5.8.0

3.0.2
-----
Fix: An item on the front-end javascript was not being localized.

3.0.1
-----
New: Option to control whether FAQs created from inside the ticket page should be set to PUBLISH or DRAFT.

3.0.0
-----
New: Requires Awesome Support 4.4.0 or later
New: Updates to support 4.4.0 UI changes
New: Added options to control which roles can see the QUICK FAQ LINKS tab

2.0.0
-----
New: Allow user to change foreground and background color of live search results
New: Support new tabs in the ticket reply area
New: Requires 4.1.0 of Awesome Support
Tweak: New more modern "look" for the live search results
Tweak: Changed error message in admin javascript to referene FAQ instead of another AS product.


1.0.7
-----
Fix: Force use of HTTPS when FORCE_SSL_ADMIN is set to true