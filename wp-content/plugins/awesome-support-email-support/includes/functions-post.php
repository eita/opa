<?php
/**
 * Post Functions.
 *
 * @package   Awesome Support E-Mail Support/Admin
 * @author    Julien Liabeuf <julien@liabeuf.fr>
 * @license   GPL-2.0+
 * @link      http://themeavenue.net
 * @copyright 2014 ThemeAvenue
 */

function wpas_mail_get_messages( $limit = -1 ) {

	$args = array(
		'post_type'              => 'wpas_unassigned_mail',
		'post_status'            => 'publish',
		'order'                  => 'DESC',
		'orderby'                => 'date',
		'posts_per_page'         => $limit,
		'no_found_rows'          => false,
		'cache_results'          => true,
		'update_post_term_cache' => true,
		'update_post_meta_cache' => true,
	);
	
	$query = new WP_Query( $args );
	
	if ( empty( $query->posts ) ) {
		return array();
	}

	return $query->posts();

}

function wpas_mail_count_messages() {

	$args = array(
		'post_type'              => 'wpas_unassigned_mail',
		'post_status'            => 'publish',
		'order'                  => 'DESC',
		'orderby'                => 'date',
		'posts_per_page'         => -1,
		'no_found_rows'          => true,
		'cache_results'          => false,
		'update_post_term_cache' => false,
		'update_post_meta_cache' => false,
	);
	
	$query = new WP_Query( $args );
	
	return $query->post_count;

}

function wpas_mail_add_message( $args ) {

	$defaults = array(
		'post_content'   => '',
		'post_title'     => '',
		'post_status'    => 'publish',
		'post_type'      => 'wpas_unassigned_mail',
		'post_author'    => '',
		'ping_status'    => 'closed',
		'post_parent'    => 0,
		'comment_status' => 'closed',
	);

	$args = apply_filters( 'wpas_mail_insert_post_args', wp_parse_args( $args, $defaults ) );

	if ( ! boolval( wpas_get_option( 'as_es_save_empty_unasgn', true ) ) ) {
		/* Do not save empty messages to unassigned */
		if ( empty( $args['post_content'] ) ) {
			return false;
		}
	}

	if ( empty( $args['post_author'] ) || false === get_user_by( 'id', $args['post_author'] ) ) {
		return false;
	}

	// Attempt to insert into database
	$return = wp_insert_post( $args, true );
	
	// Evaluate any potential errors...
	if ( is_wp_error( $return ) ) {
		
		// Try again with explicit conversion to utf-8 since most 
		// errors are because of character set conversion issues...
		$char_set_conversion = wpas_get_option( 'as_es_char_set_conversion' );		
		if ( empty( $char_set_conversion ) ) {
			$char_set_conversion = 'UTF-8';
		}	
		
		$args['post_content'] = mb_convert_encoding( $args['post_content'], $char_set_conversion );
		
		return wp_insert_post( $args, false );
		
	} else {
		// post successfully added
		return $return ;
	}

}

add_action( 'before_delete_post', 'wpas_mail_delete_unassigned_media' );
/**
 * Delete media in media library when an unassigned email is deleted.
 *
 * @since 5.4.0
 *
 * @return void
 */
function wpas_mail_delete_unassigned_media( $post_id ) {

	/* Bail out if we're not dealing with an unassigned message */  
    if ( get_post_type( $post_id ) !== 'wpas_unassigned_mail') return;
	
	/* Bail if deleting unassigned media is not allowed */
	if ( ! boolval( wpas_get_option( 'as_es_delete_unassigned_media', true ) ) ) return ;

	/* Get attachment references */
    $attachments = get_posts( array(
        'post_type'      => 'attachment',
        'posts_per_page' => -1,
        'post_status'    => 'any',
        'post_parent'    => $post_id
    ) );

	/* Delete attachment references */
    foreach ( $attachments as $attachment ) {
        if ( false === wp_delete_attachment( $attachment->ID ) ) {			
            // @TODO:Log failure to delete attachment.
        }
    }
}

