# README #

This project is an extension designed to run on the AWESOME SUPPORT WordPress Plugin Platform.  It creates tickets or updates tickets via email instead of forcing users to login and update their tickets.  

### How do I get set up? ###

Installation is straightforward using the usual WordPress Plugins -> Add New procedure.

- Download the file from your receipt or from your dashboard(Awesome-Support-Email-Support.zip).
- Within WordPress Dashboard, click `Plugins` -> `Add New`
- Click the `Upload` button and select the ZIP file you just downloaded.
- Click the `Install` button

Further Configuration and setup instructions located at: https://getawesomesupport.com/documentation/email-support/

### Change Log  ###
5.7.3
------
Tweak: Special handling for audio files when the filename is not specifed in the incoming message.

5.7.2
------
New: Added support for ticket types
Fix: Changed up the way we get the contenttransfertype to handle a situation at a client where some emails came in with an arrayiterator for the header contenttransfertype property. 
Fix: Improved the detection of base64 strings
Tweak: Added links to our trouble-shooting page 

5.7.1
------
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Warning: Breaking Change - The minimum required version of Awesome Support is now 5.7.1!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Potential Fix: For a PHP Fatal error: Cannot use isset()

5.7.0
-----
New: Added option to set 'from' encoding if not set by the incoming email.  This is experimental  and intended to be used as a last resort since there are many side-effects.
Tweak: Verify file name extensions match the incoming type (eg: image/png should have a png extension).  If not, add the extension. Applies to PNG and JPG images only.
Tweak: Some minor code optimizations.
Dev: New tracking options for when wp-debug is turned on.  Use define ('WPAS_EMAIL_LOG', true ) in wp-config to enable this tracing.
Tweak: Process multi-part/alternative parts recursively instead of stopping the process there.

5.6.1
-----
Fix: Forcibly set the character set to utf-8 when using the quoted_printable_decode function.  Workaround for PHP not setting the character-set on the variable to UTF-8.

5.6.0
-----
Update: Updating dependent packages to their latest versions (zend-framework mail and such).

5.5.6
-----
New: Breaking Change - The minimum required version of Awesome Support is now 5.7.0!
Fix: Handle multipart/mixed and multipart/parallel sections of emails as well cas multipart/digest and message/rfc822 part types.
Tweak: Explicitly handle image/png and image/jpg parts instead of letting it fall through to the default handler which might sometimes set them as '.txt' files.

5.5.5
-----
Fix: Get base64 encoding from multi-part emails
Tweak: Make sure postmetas are updated with email headers for new tickets


5.5.4
-----
New: Minimum required version of Awesome Support is now 5.2.0
Tweak:  Added a description to the password field so users can know what characters are legal.

5.5.3
-----
Fix:  A variable was not declared before being used in a filter
Fix:  a base64 function check wasn't working as intended which means certain emails were being base64 decoded into nonsensical characters.  Replaced it with what should be a better check.

5.5.2
-----
- Tweak: Added inbox id to tickets metadata when imported from a different mailbox other than the default
- Tweak: Make more attempts to remove the original message from the reply by using the specially formatted ticket # as a delimiter.
- Tweak: Added a warning to the settings screen about using the TABLE tag when enabling HTML tags and the PURIFIER

5.5.1
-----
- New: Added option to fine-tune css attributes when using html purifier.
- Tweak: Default set of html purifier tags have been widened to include more tags.

5.5.0
-----
- New: Added an option to NOT strip HTML tags out of emails at all.  This is likely to cause rendering issues for tickets but some customers still requested it.  Caveat Emptor!

5.4.1
-----
- Fix: When adding messages to the unassigned folder, they would sometimes fail silently because of character conversion issues.

5.4.0
-----
- New: Added option to delete unassigned email media when unassigned messages are deleted.
- New: Added an ultra long interval (3+ years) as a CRON interval option.  This simulates a NEVER or DO NOT RUN option.

5.3.0
-----
- New: Added some exception handling options in settings and created a better exception handling process that reduces the number of times a bad message will cause all emails to fail
- New: Added an option to control how to handle empty emails when adding to the unassigned folder.
- Tweak: Cleaned up some internal code documentation items

5.2.2
-----
- New: do_action hooks after rules update a ticket.  New hook names are: wpas_email_after_rules_update and wpas_email_after_inbox_defaults_update.

5.2.1
-----
- Tweak: Added a TRIM statement around some regex expressions to make sure that whitespace and other non-visible characters are removed when using multiple regex.
- Tweak: Internal tweak - added the custom post types for inbox and rules engine to the list of awesome support plugin pages using the wpas_plugin_post_types filter.

5.2.0
-----
- New: If the reply-to email header is set, use it as the incoming user address (experimental)
- Enh: New version of the HTMLPurifier library (4.10).

5.1.3
-----
- Fix: Check if a value exists in a variable before using it. (Thanks to George Lagonikas of subscriptiongroup.co.uk for submitting this fix.)

5.1.2
-----
- Fix: Default inbox agents and agents in email rules were sometimes not assigned properly on new tickets.  Using the wpas_new_ticket_agent_id filter to make the assignment more robust.

5.1.1
-----
- Fix: Properly flag email replies that are from other agents instead of tagging them as unknown
- Fix: When converting an unassigned email to a ticket or ticket reply, make sure that attachments are attached to the ticket and not left unattached in the media library.

5.1.0
-----
- New: Added option to turn off automatic clean-up of email footers.  
- New: Option to specify one or more regex functions to clean up email footers
- New: Option to specify one or more regex functions to replace data in incoming emails. This is useful if you want to mask credit card information for example.
- Fix: Issue with emails getting cut off when the word "on" was encountered. It was an issue with attempting to clean the footer in gmail messages. 

5.0.3
-----
- Fix: When adding users there were some instances where the firstname would be blank causing email imports to fail.
- Fix: Incorrect description under the public/private flag in the inbox rules configuration screen.
- Fix: Grammar label issue on rules screen 

5.0.2
-----
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Warning: Breaking Change - You must update your email inbox configuration to explicitly choose the security protocol.
!          Go to TICKETS->SETTINGS->EMAIL PIPING and verify the SECURE PORT option. Generally it should be set to SSL or NONE.
!
!		   If using more than one mailbox then you also need to go to TICKETS->INBOX CONFIGURATIONS and update your mailboxes there as well.
!
! Additionally, you must use Awesome Support 4.1.0 or later!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

- New: Option to send notifications for unassigned emails/tickets
- New: Added options in rules to UPDATE and CLOSE ticket in one action
- New: Experimental support for TLS
- New: Option to not import duplicate emails
- Tweak: Run update rules earlier in the ticket insert process by calling an earlier filter.  This allows auto agent assignment to get data it needs since the filters/hooks it uses runs later.
- Fix: Encoding issue - when email arrived with mixed encoded characters an error was thrown when the TICKETS->SETTINGS->EMAIL PIPING screen encoding settings were blank.
- Fix: Inbox defaults and rule defaults weren't working properly because a core function was overwriting their values.  Fixed by increasing the numeric value of the priorities used.
- Tweak: Includes new version of ZENDFRAMEWORK library (as of 5.0.2 RC02)
- Tweak: Add option to turn off email rejection notifications

5.0.1
-----
- ENH: New options to control the user name when a user has to be created.  Requires AS 4.0.3 or later.
- ENH: Added support for emails where the entire email is encoded in base64 without providing an additional part with ascii/plain or html encoding.
- ENH: Option to allow certain html tags to be retained (BETA version - and possibly insecure if enabled since users can retain javascript embedded in emails!)
- ENH: Added an option to specify a character set for all emails.  This allows for the character set to be matched to the MYSQL server but could result in the loss of data for strings that cannot be converted.
- Fix: Styling issues on inbox rules and multi-inbox config pages
- Tweak: Better logging of rejected emails to the log files.
- Fix: Various bug fixes, removing unused code and a bit of code cleanup.
- Fix: Mailbox rules were unnecessarily overwriting the mailbox defaults for things like priority.  
- Fix: Handle emails where no transfer encoding is specified but the actual encoding is base64.
- Fix: Handle emails where the encoding is base64
- Fix: Email rules could not handle spaces in the "rule" field (aka the $rule_contents variable) because it was being sanitized using FILTER_SANITIZE_URL instead of FILTER_SANITIZE_STRING

5.0.0
-----
- New: Changed library used to talk to mailboxes from FLOURISH to ZEND
- Tweak: Changed version numbers to move the primary version to the first digit in the version number (from the second digit)
- New: Min PHP version is 5.6
- New: Multiple Mailboxes
- New: Rules engine for incoming emails
	- Prevent emails from being added
	- Prevent emails from being deleted
	- Update priority/dept/product/etc based on rules for newly added tickets/replies
	- Create custom commands that can be sent via email

**NOTE:** Requires 4.0.0 of the core Awesome Support plugin!	


0.4.0 (internal build)
-----
- Enhancement: Easier to re-assign unassigned emails
  - Shows both the agent and ticket # field so user can decide if the message is a new ticket or a reply to an existing ticket.  Before, the system tried to be the one that figured it out but didn't always do a good - job of that.
  - Allows user to save agent and/or ticket number without exiting the unassigned ticket.  Before, simply choosing one or the other automatically exited the unassigned ticket and could sometimes lead to the user not - realizing where the message was assigned to.
Enhancement: Always assign replies to a ticket number regardless of which email address the reply arrived from.  This allows for users to send replies from any email address since many users today have multiple email - addresses.
	- NOTE: Unrecognized email addresses will treat the reply as if its coming from the client/ticket creator.  So if an AGENT uses an unrecognized email address it will show up as being from the customer instead.
- Enhancement: Properly attribute emailed replies to the primary agent or the new agents in 3.6.0 of core (secondary or tertiary agents)
- Enhancement: Properly attribute emailed replies to the the new "interested third parties" added in 3.6.0 of core
- New: Uses a new capability to control who can see "unassigned" tickets (view_unassigned_tickets).  Added by default to administrators only in core.
- New: Fills in the channel field in the upcoming 4.0.0 version of Core.
- New: Requires 4.0.0 of core to function
- New: New filter to allow better access to the raw email.
- New: Added support for the ticket lock feature in the forthcoming PRODUCTIVITY add-on.
- New: Added new hook after data is saved - this will help to trigger notifications at the right time (when all data is saved instead of when only some is saved)
- Tweak: Adjusted some logic so that emails can be sent out after all data is saved (including attachments)
- Tweak: Added product name to license warning so user can tell which add-on is generating the warning.
- Tweak: Some grammar changes

**NOTE:** Requires 4.0.0 of the core Awesome Support plugin!

0.3.0 
------
- Add: Final - email attachments support. 
- Add: Choice of "heartbeat" or "wp-cron" method of scheduling checking for emails.  
  - The new default is wp-cron which does NOT show an alert on the dashboard when wp-admin is open.
  - But it fires more often and more consistently.
  - To see the message and to use the FETCH button, change to the jQuery Hearbeat method and save. 
- Add: Option to check for messages every 60 seconds.

0.2.9 (Internal Build)
-----
- Add: Continue developmment of email attachments support. 

0.2.8 (Internal Build)
-----
- Add: Start adding email attachments support. 

0.2.7
-----
- Fix: Obscure Flourish bug that caused the auto-create user functionality to create users with the wrong email address.

0.2.6
-----
- Update: Clean up translation catalog, textdomains and add to POEDITOR.com
- Update: Make the delimiter that shows where a reply starts and end translatable.
- Fix: Force use of HTTPS when FORCE_SSL_ADMIN is set to true

0.2.5
-----
- Better handling of replies to closed tickets. There are options now to reject replies sent to close tickets or to accept the reply and reopen the ticket.

0.2.4
-----
- The email support extension now has three options for handling unrecognized emails:
     1. Leave them in the unassigned folder like its done now
     2. Create a new ticket and, if necessary, a new user based on email address.  Send new user link to reset password
     3. Create a new ticket if the email address is recognized; otherwise leave in the unassigned folder.
- These new options are located in the TICKETS->SETUP->EMAIL PIPING tab

0.2.3
-----
- Add new filters
- Filter the returned user id
- Make sure the sub-menu is defined before working on it
- Update incorrect links in settings page