<?php


$emails = new WPAS_Email_Notification( $post_id );
		
$responses = wpas_cr_get_canned_responses();
		
$button_responses = wpas_cr_get_button_canned_responses();


if( empty( $responses ) ) {
	return;
}




$categories = get_terms( 'wpas_cr_category', array( 'hide_empty' => true ) );
$tags = get_terms( 'wpas_cr_tag', array( 'hide_empty' => true ) );



printf( '<input type="hidden" value="%s" id="wpas_cr_get_canned_response_content_nonce" />', wp_create_nonce( 'wpascr_get_canned_response_content' ) );

?>

<div class="heading wpas_cr_main_heading"><?php _e( 'Canned Responses', 'as-canned-responses' ); ?></div>


<div class="wpas_cr_loading"></div>

<div class="clear clearfix"></div>

<div class="wpas_cr_canned_responses_filters">

	<div class="wpas_cr_search_canned_responses_wrapper">

		<select class="wpas-select2 wpas_cr_canned_responses_dropdown">
			<?php
			printf( '<option value="&nbsp;">%s</option>', esc_html__( 'Select or Search for a Canned Response', 'as-canned-responses' ) );

			foreach ( $responses as $response ) {

				$title   = esc_attr( $response->post_title );
				
				printf( '<option value="%s">%s</option>', $response->ID, $title );

			}

			?>

		</select>
	</div>
	

	<?php if( !empty( $categories ) || !empty( $tags ) ) { ?>
	<div class="wpas_cr_taxonomy_filters">

		<?php if( !empty( $tags ) ) { ?>
		<div class="filter_field">
			<label><strong><?php _e( 'Filter One-click Responses By Tag', 'as-canned-responses' ); ?></strong></label>

			<?php
				wp_dropdown_categories( array(
					'name'				=> 'wpas_cr_tag',
					'taxonomy'			=> 'wpas_cr_tag',
					'option_none_value'	=> '',
					'hide_if_empty'		=> true ,
					'show_option_none'	=> __( 'Select a Tag', 'as-canned-responses' ),
					) );
			?>
		</div>

		<?php 
		}
		if( !empty( $categories ) ) { ?>
		<div class="filter_field">
			<label><strong><?php _e( 'Filter One-click Responses By Category', 'as-canned-responses' ); ?></strong></label>

			<?php
				wp_dropdown_categories( array(
					'name'				=> 'wpas_cr_category',
					'taxonomy'			=> 'wpas_cr_category',
					'option_none_value'	=> '',
					'hide_if_empty'		=> true ,
					'show_option_none'	=> __( 'Select a Category', 'as-canned-responses' ),
					) );
				?>

		</div>

		<?php } ?>

	</div>

	<?php } ?>
	
</div>


<div class="wpas-canned-responses-wrap">
	<div class="wpas_cr_canned_responses_buttons">
		<?php echo wpas_cr_canned_responses_tags( $button_responses, $post_id ); ?>
	</div>
</div>

<div class="clear clearfix"></div>