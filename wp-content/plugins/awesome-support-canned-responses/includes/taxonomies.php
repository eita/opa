<?php

/**
 * Register Canned Response category taxonomy
 */
function wpas_cr_register_category_taxonomy() {
		
	$args = array(
		'public' => true,
		'labels' => array(
			'name'			=> __( 'Category',			'as-canned-responses' ),
			'singular_name' => __( 'Category',			'as-canned-responses' ),
			'menu_name'		=> __( 'Categories',		'as-canned-responses' ),
			'search_items'	=> __( 'Search Category',	'as-canned-responses' ),
			'popular_items' => __( 'Categories',		'as-canned-responses' ),
			'all_items'		=> __( 'All Categories',	'as-canned-responses' ),
			'edit_item'		=> __( 'Edit Category',		'as-canned-responses' ),
			'update_item'	=> __( 'Update Category',	'as-canned-responses' ),
			'add_new_item'	=> __( 'Add New Category',	'as-canned-responses' ),
			'new_item_name' => __( 'New Category Name', 'as-canned-responses' ),
			'separate_items_with_commas'	=> __( 'Separate Categories with commas',		  'as-canned-responses' ),
			'add_or_remove_items'			=> __( 'Add or remove Categories',				  'as-canned-responses' ),
			'choose_from_most_used'			=> __( 'Choose from the most popular Categories', 'as-canned-responses' ),
		),
		'show_ui'               => true,
		'show_admin_column'		=> true,
		'hierarchical'			=> true,
		'capabilities'			=> array(
			'manage_terms' => 'administer_awesome_support'
		)
	);
	
	register_taxonomy( 'wpas_cr_category', 'canned-response', $args );
}
	
/**
 * Register Canned Response tag taxonomy
 */
function wpas_cr_register_tag_taxonomy() {
		
	$args = array(
		'public' => true,
		'labels' => array(
			'name'			=> __( 'Tag',				'as-canned-responses' ),
			'singular_name' => __( 'Tag',				'as-canned-responses' ),
			'menu_name'		=> __( 'Tags',				'as-canned-responses' ),
			'search_items'	=> __( 'Search Tags',		'as-canned-responses' ),
			'popular_items' => __( 'Tags',				'as-canned-responses' ),
			'all_items'		=> __( 'All Tags',			'as-canned-responses' ),
			'edit_item'		=> __( 'Edit Tag',			'as-canned-responses' ),
			'update_item'	=> __( 'Update Tag',		'as-canned-responses' ),
			'add_new_item'	=> __( 'Add New Tag',		'as-canned-responses' ),
			'new_item_name' => __( 'New Tag Name',		'as-canned-responses' ),
			'separate_items_with_commas' => __( 'Separate Tags with commas',			'as-canned-responses' ),
			'add_or_remove_items'		 => __( 'Add or remove Tags',					'as-canned-responses' ),
			'choose_from_most_used'		 => __( 'Choose from the most popular Tags',	'as-canned-responses' ),
		),
		'show_ui'               => true,
		'show_admin_column'		=> true,
		'capabilities'			=> array(
			'manage_terms' => 'administer_awesome_support'
		)
	);
	
	register_taxonomy( 'wpas_cr_tag', 'canned-response', $args );
}
    
	
	

add_action( 'init', 'wpas_cr_init_taxonomies' );

/**
 * Register all taxonomies
 */
function wpas_cr_init_taxonomies() {
	
	wpas_cr_register_category_taxonomy();
	wpas_cr_register_tag_taxonomy();

}