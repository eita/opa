<?php

add_action( 'init',        'wpas_cr_register_post_type',     10, 0 );

function wpas_cr_register_post_type() {
		$labels = array(
			'name'               => _x( 'Canned Responses', 'post type general name', 'as-canned-responses' ),
			'singular_name'      => _x( 'Canned Response', 'post type singular name', 'as-canned-responses' ),
			'menu_name'          => _x( 'Canned Responses', 'admin menu', 'as-canned-responses' ),
			'name_admin_bar'     => _x( 'Canned Response', 'add new on admin bar', 'as-canned-responses' ),
			'add_new'            => _x( 'Add New', 'book', 'as-canned-responses' ),
			'add_new_item'       => __( 'Add New Canned Response', 'as-canned-responses' ),
			'new_item'           => __( 'New Canned Response', 'as-canned-responses' ),
			'edit_item'          => __( 'Edit Canned Response', 'as-canned-responses' ),
			'view_item'          => __( 'View Canned Response', 'as-canned-responses' ),
			'all_items'          => __( 'All Canned Responses', 'as-canned-responses' ),
			'search_items'       => __( 'Search Canned Responses', 'as-canned-responses' ),
			'parent_item_colon'  => __( 'Parent Canned Response:', 'as-canned-responses' ),
			'not_found'          => __( 'No canned responses found.', 'as-canned-responses' ),
			'not_found_in_trash' => __( 'No canned responses found in Trash.', 'as-canned-responses' )
		);

		/* Post type capabilities */
		$cap = array(
			'read'					 => 'view_ticket',
			'read_post'				 => 'view_ticket',
			'read_private_posts' 	 => 'view_private_ticket',
			'edit_post'				 => 'edit_ticket',
			'edit_posts'			 => 'edit_ticket',
			'edit_others_posts' 	 => 'edit_other_ticket',
			'edit_private_posts' 	 => 'edit_private_ticket',
			'edit_published_posts' 	 => 'edit_ticket',
			'publish_posts'			 => 'create_ticket',
			'delete_post'			 => 'delete_ticket',
			'delete_posts'			 => 'delete_ticket',
			'delete_private_posts' 	 => 'delete_private_ticket',
			'delete_published_posts' => 'delete_ticket',
			'delete_others_posts' 	 => 'delete_other_ticket'
		);

		$args = array(
			'labels'              => $labels,
			'public'              => false,
			'publicly_queryable'  => false,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'query_var'           => true,
			'rewrite'             => array( 'slug' => 'canned-response' ),
			'capability_type'     => 'edit_ticket',
			'capabilities'        => $cap,
			'has_archive'         => false,
			'hierarchical'        => false,
			'menu_position'       => null,
			'supports'            => array( 'title', 'editor' ),
			'can_export'          => true,
			 'exclude_from_search'=> true,
		);

		register_post_type( 'canned-response', $args );
	}
	
	
	
/**
 * Return canned responses
 * 
 * @param array $args
 * 
 * @return array
 */
function wpas_cr_get_canned_responses( $args = array() ) {
	

	$defaults = array(
		'post_type'             => 'canned-response',
		'post_status'           => 'publish',
		'posts_per_page'        => -1,
		'no_found_rows'         => true,
		'orderby' 				=> 'title',
		'order'   				=> 'ASC',
		'tax_query'				=> array(),
		'meta_query'			=> array()
	);
	
	
	$args = wp_parse_args( $args, $defaults );
	
	$query = new WP_Query( $args );

	if ( empty( $query->posts ) ) {
		return array();
	} 
	
	return $query->posts;
}


/**
 * Return canned responses for buttons
 * 
 * @return array
 */
function wpas_cr_get_button_canned_responses() {
	return wpas_cr_get_canned_responses( array( 
		'meta_query' => array(
			'relation' => 'OR',
			array(
				'key' => 'is_button',
				'value'	=> '',
				'compare' => 'NOT EXISTS'
				),
				array(
				'key' => 'is_button',
				'value' => 1
				)
		)
	) );
}



	
add_action( 'add_meta_boxes', 'wpas_cr_metaboxes' );


/**
 * Register metaboxes for canned responses.
 * 
 * @global type $pagenow
 */
function wpas_cr_metaboxes() {
	
	add_meta_box( 'wpas-cr-settings-mb', __( 'Settings', 'as-canned-responses' ), 'wpas_cr_metabox_settings', 'canned-response', 'side', 'low' );
	
}


/**
 * Canned response settings metabox callback
 * 
 * @global int $post_id
 */
function wpas_cr_metabox_settings() {
	global $post_id;

	$color = '';

	
	if ( $post_id ) {
		$color = get_post_meta( $post_id, 'color', true );
		$is_button =  get_post_meta( $post_id, 'is_button', true );
	} else {
		$is_button = true;
	}
	
	
	wp_nonce_field( 'wpas_cr_nonce', 'wpas-cr-settings-nonce', true, true );
	
	?>


	<div>
		<p>
			<label><input type="checkbox" name="is_button" value="1" <?php checked( 1, $is_button ); ?> /> <strong><?php _e( 'Show Button', 'as-canned-responses' ) ?> </strong></label>
		</p>
	</div>

	<div>
		<p>
			<label><strong><?php _e( 'Color', 'as-canned-responses' ) ?> </strong></label>
			<input type="text" name="color" value="<?php echo $color; ?>" class="wpas-cr-color-field" />
		</p>
	</div>

	<?php
	
}


add_action( 'save_post_canned-response', 'wpas_cr_save_canned_response' );

/**
 * Save Canned Response post
 * 
 * @param int $post_id
 * 
 * @return void
 */
function wpas_cr_save_canned_response( $post_id ) {

	// Verify nonce
	if ( ! ( isset( $_POST['wpas-cr-settings-nonce'] ) && wp_verify_nonce( $_POST['wpas-cr-settings-nonce'], 'wpas_cr_nonce' ) ) ) {
		return;
	}

	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	$post = get_post( $post_id );
	
	
	
	if ( 'canned-response' == $_POST['post_type'] && $post->post_type == 'canned-response' ) {
		
		$color		= filter_input( INPUT_POST, 'color', FILTER_SANITIZE_STRING );
		$is_button  = filter_input( INPUT_POST, 'is_button', FILTER_SANITIZE_NUMBER_INT );
		
		
		update_post_meta( $post_id, 'color', $color );
		update_post_meta( $post_id, 'is_button', $is_button );
	}

}


add_filter( 'manage_edit-canned-response_sortable_columns', 'wpas_cr_canned_response_sortable_columns' );

/**
 * Making category and tag column sortable for canned responses
 * 
 * @param array $columns
 * 
 * @return array
 */
function wpas_cr_canned_response_sortable_columns( $columns ) {
	
    $columns['taxonomy-wpas_cr_tag']		= 'wpas_cr_tag';
	$columns['taxonomy-wpas_cr_category']	= 'wpas_cr_category';
	
    return $columns;
}


add_action( 'wp_ajax_wpas_cr_get_canned_response_buttons', 'wpas_cr_get_canned_response_buttons' );

/**
 * Return canned responses buttons based on category or tag via ajax
 */
function wpas_cr_get_canned_response_buttons() {
	
	$cat		= filter_input( INPUT_POST, 'category',  FILTER_SANITIZE_NUMBER_INT );
	$tag		= filter_input( INPUT_POST, 'tag',		 FILTER_SANITIZE_NUMBER_INT );
	$ticket_id  = filter_input( INPUT_POST, 'ticket_id', FILTER_SANITIZE_NUMBER_INT );
	
	$tax_query = array();
	
	
	if( $cat && $cat > 0 ) {
		$tax_query[] = array(
			'taxonomy' => 'wpas_cr_category',
			'field'    => 'term_id',
			'terms'    => array( (int) $cat ),
		);
	}
	
	if( $tag && $tag > 0 ) {
		$tax_query[] = array(
			'taxonomy' => 'wpas_cr_tag',
			'field'    => 'term_id',
			'terms'    => array( (int) $tag ),
		);
	}
	
	
	if(!empty( $tax_query ) ) {
		$tax_query['relation'] = 'OR';
	}
	
	
	$args = array(
		'tax_query' => $tax_query
	);
	
	
	$responses = wpas_cr_get_canned_responses( $args );
	$tags = wpas_cr_canned_responses_tags( $responses, $ticket_id );
	wp_send_json_success( array( 'buttons' => $tags ) );
}


/**
 * Generate buttons for canned responses
 * 
 * @param array $responses
 * @param int $post_id
 * 
 * @return string
 */
function wpas_cr_canned_responses_tags( $responses, $post_id ) {
			
	$emails    = new WPAS_Email_Notification( $post_id, true );
	
	$tags = array();
	
	foreach ( $responses as $response ) {
		$title   = esc_attr( $response->post_title );

		$color = get_post_meta( $response->ID,  'color', true );

		$style = "background:{$color};";
		
		if( $color ) {
			$style .= "color:#FFF;";
		}
		
		
		$tags[] = sprintf( '<span class="wpas-canned-response wpas-label" data-id="%s" style="%s">%s</span>', $response->ID, $style, $title );
	}
	
	return implode( "\n", $tags );
}


/**
 * Return canned response tag
 * 
 * @param int $post_id
 * 
 * @return int
 */
function wpas_cr_get_tag( $post_id) {
	
	$tag = wp_get_object_terms( $post_id, 'wpas_cr_tag' );
	if( !empty( $tag ) && is_array( $tag ) ) {
		$tag = $tag[0]->term_id;
	}
	
	return $tag;
}


/**
 * Return canned response category
 *  
 * @param int $post_id
 * 
 * @return int
 */
function wpas_cr_get_category( $post_id ) {
	
	$category = wp_get_object_terms( $post_id, 'wpas_cr_category' );
	if( !empty( $category ) && is_array( $category ) ) {
		$category = $category[0]->term_id;
	}
	
	return $category;
}