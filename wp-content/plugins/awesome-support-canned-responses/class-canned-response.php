<?php
/**
 * Canned Response
 *
 * @package   Awesome Support Canned Responses
 * @author    Awesome Support <contact@getawesomesupport.com>
 * @license   GPL-2.0+
 * @link      https://getawesomesupport.com
 * @copyright 2016 Awesome Support
 */

class WPASCR_Canned_Response {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 * @var      object
	 */
	protected static $instance = null;

	protected $responses = null;
	protected $post_id = null;

	public function __construct() {

		require_once WPASCR_PATH . 'includes/post_types.php';
		require_once WPASCR_PATH . 'includes/taxonomies.php';
		
		
		add_action( 'admin_footer',         array( $this, 'add_menu_script' ),        10, 0 );
		add_filter( 'wpas_addons_licenses', array( $this, 'addon_license' ),          10, 1 );
		add_filter( 'wpas_plugin_settings', array( $this, 'add_general_settings' ),	  10, 1 );

		// Load the plugin translation.
		add_action( 'plugins_loaded', array( $this, 'load_plugin_textdomain' ), 15 );

		if ( isset( $_GET['post'] ) ) {

			$this->post_id = intval( $_GET['post'] );

			add_filter( 'contextual_help',          array( $this, 'contextual_help' ), 10, 0 );
			add_filter( 'wpas_admin_tabs_after_reply_wysiwyg', array( $this , 'add_tab' ) , 13, 1 ); // Add tab for canned responses
			add_filter( 'wpas_admin_tabs_after_reply_wysiwyg_canned_responses_content', array( $this, 'tab_content' ) , 11, 1 ); // Add content for canned responses tab

		}
		
		add_action( 'admin_enqueue_scripts',    array( $this, 'enqueue_styles' ), 10, 0 );
		add_action( 'admin_enqueue_scripts',    array( $this, 'enqueue_scripts' ), 10, 0 );
		
		add_action( 'wp_ajax_wpas_cr_get_canned_response_content', array( $this, 'ajax_get_canned_response_content' ) );
	}
	
	/**
	 * Return an instance of this class.
	 *
	 * @since     3.0.0
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Activate the plugin.
	 *
	 * The activation method just checks if the main plugin
	 * Awesome Support is installed (active or inactive) on the site.
	 * If not, the addon installation is aborted and an error message is displayed.
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public static function activate() {

		if ( !class_exists( 'Awesome_Support' ) ) {
			deactivate_plugins( basename( __FILE__ ) );
			wp_die(
				sprintf( __( 'You need Awesome Support to activate this addon. Please <a href="%s" target="_blank">install Awesome Support</a> before continuing.', 'as-canned-responses' ), esc_url( 'https://getawesomesupport.com' ) )
			);
		}

	}

	/**
	 * Add license option.
	 *
	 * @since  0.1.0
	 * @param  array $licenses List of addons licenses
	 * @return array           Updated list of licenses
	 */
	public function addon_license( $licenses ) {

		$licenses[] = array(
			'name'      => __( 'Canned Responses', 'as-canned-responses' ),
			'id'        => 'license_canned_responses',
			'type'      => 'edd-license',
			'default'   => '',
			'server'    => esc_url( 'https://getawesomesupport.com' ),
			'item_name' => 'Canned Responses',
			'file'      => WPASCR_PATH . 'canned-responses.php'
		);

		return $licenses;
	}

	public function enqueue_styles() {
		wp_enqueue_style( 'wpascr-admin-style', WPASCR_URL . 'assets/css/admin.css', array(), WPASCR_VERSION, 'all' );
	}

	public function enqueue_scripts() {
		wp_enqueue_style( 'wp-color-picker' );	
		wp_enqueue_script( 'wpascr-admin-script', WPASCR_URL . 'assets/js/admin.js', array( 'jquery', 'editor', 'wp-color-picker' ), WPASCR_VERSION, true );
	}

	public function add_menu_script() {

		global $pagenow;

		if ( 'post-new.php' !== $pagenow || !isset( $_GET['post_type'] ) || 'canned-response' !== $_GET['post_type'] ) {
			return;
		}

		?><script type="text/javascript">jQuery('#menu-posts-ticket, #menu-posts-ticket > a').removeClass('wp-not-current-submenu').addClass('wp-has-current-submenu wp-menu-open');</script><?php
	}

	/**
	 * Add contextual help.
	 *
	 * The contextual help shows all the available tags
	 * and how to use them in canned responses.
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function contextual_help() {

		global $post;

		if ( isset( $_GET['post_type'] ) && 'canned-response' === $_GET['post_type'] || isset( $post ) && 'canned-response' === $post->post_type ):

			/**
			 * Gather the list of e-mail template tags and their description
			 */
			$emails    = new WPAS_Email_Notification( false );
			$list_tags = $emails->get_tags();

			$tags = '<table class="widefat"><thead><th class="row-title">' . __( 'Tag', 'as-canned-responses' ) . '</th><th>' . __( 'Description', 'as-canned-responses' ) . '</th></thead><tbody>';

			foreach ( $list_tags as $the_tag ) {
				$tags .= '<tr><td class="row-title"><strong>' . $the_tag['tag'] . '</strong></td><td>' . $the_tag['desc'] . '</td></tr>';
			}

			$tags .= '</tbody></table>';
			
			$screen = get_current_screen();
			
			$screen->add_help_tab( array(
				'id'      => 'template-tags',
				'title'   => __( 'Template Tags', 'as-canned-responses' ),
				'content' => sprintf( __( '<p>When setting up your canned responses, you can use a certain number of template tags allowing you to dynamically add ticket-related information at the moment the reply is sent. Here is the list of available tags:</p>%s', 'as-canned-responses' ), $tags )
			) );

			return true;

		else:
			return false;
		endif;
	}


	public function get_canned_responses_markup() {

		$post_id = $this->post_id;
		
		ob_start();
		
		include WPASCR_PATH . 'includes/canned_responses_tab.php';

		return ob_get_clean();
	}
	
	/**
	 * Add tab for canned responses
	 * 
	 * @param array $tabs
	 * 
	 * @return array
	 */
	public function add_tab( $tabs ) {
		
		// Check to see if the option to show the Canned Response tab is turned on.
		if ( false === boolval( wpas_get_option( 'enable_canned_response_links', true ) ) ) {
			return $tabs;
		}
		
		// Ok, agents can see the Canned Response tab but certain roles might still be excluded - check for those here.
		if ( wpas_current_role_in_list( wpas_get_option( 'hide_canned_response_roles' ) ) ) {
			return $tabs;
		}						
		
		$tabs['canned_responses'] = __( 'Canned Responses', 'as-canned-responses' );
	
		return $tabs;
	}
	
	/**
	 * 
	 * Add global custom fields settings section
	 * 
	 * @param array $settings
	 * 
	 * @return array
	 */
	function add_general_settings( $def ) {
		
		$settings = array(
			'canned_response_settings' => array(
				'name'    => __( 'Canned Response', 'as-canned-responses' ),
				'options' => array(
					array(
						'name'    => __( 'General', 'as-canned-responses' ),
						'type'    => 'heading',
						'desc'    => __( 'General configuration options for canned responses', 'as-canned-responses' ),
					),
					
					array(
						'name'    => __( 'Enable the Canned Response Tab', 'wpas-documentation' ),
						'id'      => 'enable_canned_response_links',
						'type'    => 'checkbox',
						'desc'    => __( 'Enable the Canned Responses tab when an agent is replying to a ticket', 'wpas-documentation' ),
						'default' => true,
					),
					array(
						'name'    => __( 'Roles That Are NOT Allowed To See The Canned Response Tab', 'wpas-documentation' ),
						'id'      => 'hide_canned_response_roles',
						'type'    => 'text',
						'desc'    => __( 'Enter a comma separated list of roles that should not see the QUICK DOCUMENTATION LINKS tab. Roles should be the internal WordPress role id such as wpas_support_agent and are case sensitive. There should be no spaces between the commas and role names when entering multiple roles.', 'wpas-documentation' ),
						'default' => ''
					),			
				)
			)
		);

		return array_merge( $def, $settings );

	}	

	/**
	 * Return content for canned responses tab
	 * 
	 * @return string
	 */
	public function tab_content() {
		
		ob_start();
		
		$this->display_canned_responses();
		
		return ob_get_clean();
	}

	public function display_canned_responses() {
		echo $this->get_canned_responses_markup();
	}

	/**
	 * Load the plugin text domain for translation.
	 *
	 * With the introduction of plugins language packs in WordPress loading the textdomain is slightly more complex.
	 *
	 * We now have 3 steps:
	 *
	 * 1. Check for the language pack in the WordPress core directory
	 * 2. Check for the translation file in the plugin's language directory
	 * 3. Fallback to loading the textdomain the classic way
	 *
	 * @since   0.1.2
	 * @return boolean True if the language file was loaded, false otherwise
	 */
	public function load_plugin_textdomain() {

		$lang_dir       = WPASCR_ROOT . 'languages/';
		$lang_path      = WPASCR_PATH . 'languages/';
		$locale         = apply_filters( 'plugin_locale', get_locale(), 'as-canned-responses' );
		$mofile         = "as-private-notes-$locale.mo";
		$glotpress_file = WP_LANG_DIR . '/plugins/awesome-support-canned-responses/' . $mofile;

		// Look for the GlotPress language pack first of all
		if ( file_exists( $glotpress_file ) ) {
			$language = load_textdomain( 'as-canned-responses', $glotpress_file );
		} elseif ( file_exists( $lang_path . $mofile ) ) {
			$language = load_textdomain( 'as-canned-responses', $lang_path . $mofile );
		} else {
			$language = load_plugin_textdomain( 'as-canned-responses', false, $lang_dir );
		}

		return $language;

	}
	
	
	/**
	 * Return canned response content via ajax
	 */
	function ajax_get_canned_response_content() {
		
		$canned_response_id	= filter_input( INPUT_POST, 'crid',	FILTER_SANITIZE_NUMBER_INT );
		$ticket_id			= filter_input( INPUT_POST, 'ticket_id', FILTER_SANITIZE_NUMBER_INT );

		if( !$canned_response_id || !$ticket_id || !check_ajax_referer( 'wpascr_get_canned_response_content', 'security', false ) ) {
			wp_send_json_error( array( 'message' => "You don't have access to perform this action." ) );
			die();
		}


		$canned_response = get_post( $canned_response_id );

		$content = '';

		if( $canned_response ) {
			
			$emails = new WPAS_Email_Notification( $ticket_id );
			$content = wpautop( str_replace( '\'', '&apos;', $emails->fetch( $canned_response->post_content ) ) );
		}

		wp_send_json_success( array( 'content' => $content ) );
		die();
	}

}