# README #

This project is an extension designed to run on the AWESOME SUPPORT Wordpress Plugin Platform.  

### How do I get set up? ###

Installation is straightforward using the usual WordPress Plugins -> Add New procedure.

- Download the file from your receipt or from your dashboard(Awesome-Notifications.zip).
- Within WordPress Dashboard, click `Plugins` -> `Add New`
- Click the `Upload` button and select the ZIP file you just downloaded.
- Click the `Install` button


### Change Log  ###
3.0.0
-----
New: Assign categories to a Canned response and filter by them when replying to a ticket.
New: Assign tags to a Canned response and filter by them when replying to a ticket.
New: Color-code the "buttons" for each canned response.
New: Determine if a canned response shows up in the button area when replying to a ticket
New: Search for a canned response.

2.0.0
-----
New: Requires Awesome Support 4.4.0 or later
New: Updates to support 4.4.0 UI changes
New: Added options to control which roles can see the Canned Response tab

1.4.0
-----
New: Support new tabs under reply area of Awesome Support 4.1.0
Tweak: Moving version number to semver standard making this new version 1.4.0.

0.1.3 
-----
Tweak: Removed restriction where only WPADMINs can see the CANNED RESPONSE menu option.  Now any role with create_users capability can see the menu option.
Tweak: Add index.php file to languages folder root.

0.1.2
-----
Updates: Clean up translation catalog, textdomains and add to POEDITOR.net
Fix: Force use of HTTPS when FORCE_SSL_ADMIN is set to true