jQuery(document).ready(function ($) {

	'use strict';

	/*
	References
	http://codeblow.com/questions/method-to-check-whether-tinymce-is-active-in-wordpress/
	https://wordpress.org/support/topic/tinymceactiveeditorgetcontentcontent-does-not-work-with-tinymce-advanced
	 */

	var cannedResponse, reply, replyWrap;
        reply = $('#wpas_reply');
	replyWrap = $('#wp-wpas_reply-wrap');

        function place_canned_response( cannedResponse ) {
                /* Check if canned response is empty */
		if ( cannedResponse ) {

			/* Check which editor is active TinyMCE or HTML */
			if (replyWrap.hasClass('tmce-active')) {

				/* Check if TinyMCE is loaded */
				if (typeof (tinyMCE) != 'undefined') {

					/* Check version of TinyMCE */
					if (tinymce.majorVersion < 4) {
						tinyMCE.execInstanceCommand('wpas_reply', 'mceInsertContent', false, cannedResponse);
					} else {
						tinyMCE.get('wpas_reply').insertContent(cannedResponse);
					}

				}

			} else {

				/* Append canned response to existing textarea value */
				reply.val(function (i, val) {
					return val + cannedResponse;
				});

			}
                        
                        // Activate Reply form tab
                        if( cannedResponse.trim() ) {
                                $('#wpas_admin_tabs_after_reply_wysiwyg li[rel=wpas_admin_tabs_after_reply_wysiwyg_reply_form]').trigger('click');
                        }

		} else {
			alert('This canned response is empty.');
		}
        }

        
        /**
         * Filter canned reponse buttons based on category and tag
         */
        $( '.wpas_cr_taxonomy_filters select' ).change( function( e ) {
                e.preventDefault();
                
                $( '.wpas_cr_taxonomy_filters select' ).prop( 'disabled', true );
                
                var loader = $('<span class="spinner"></span>');
                loader.css({visibility: 'visible', float : 'none' });
                
                $('.wpas-canned-responses-wrap .wpas_cr_canned_responses_buttons').html(loader);
                
                var data = {
                        action    : 'wpas_cr_get_canned_response_buttons',
                        category  : $('.wpas_cr_taxonomy_filters [name=wpas_cr_category]').val(),
                        tag       : $('.wpas_cr_taxonomy_filters [name=wpas_cr_tag]').val(),
                        ticket_id : wpas.ticket_id
                };
                
                $.post( ajaxurl, data, function (response) {
                        $( '.wpas_cr_taxonomy_filters select' ).prop( 'disabled', false );
			$('.wpas-canned-responses-wrap .wpas_cr_canned_responses_buttons').html( response.data.buttons );
		});
                
        });
        
        
        /**
         * Get canned response content
         * 
         * @param int id
         * 
         * @returns void
         */
        function wpas_get_canned_response_content( id ) {
                
                
                var loading_wrapper = $('.wpas_cr_loading');
                
                if( loading_wrapper.hasClass('loading') || !id ) {
                        return;
                }
                
                
                var loader = $('<span class="spinner"></span>');
                loader.css({visibility: 'visible', float : 'none' });
                
                
                loading_wrapper.addClass('loading').append( loader );
                
                
                var data = {
                        action    : 'wpas_cr_get_canned_response_content',
                        ticket_id : wpas.ticket_id,
                        crid      : id,
                        security  : $('#wpas_cr_get_canned_response_content_nonce').val()
                };

                $.post( ajaxurl, data, function (response) {

                        loader.remove();
                        loading_wrapper.removeClass('loading');
                        
                        place_canned_response( response.data.content );

                });
        }
        
        /**
         * Place selected canned response into reply editor once a search dropdown changed
         */
        $( '.wpas_cr_canned_responses_dropdown').change( function() {
                if( $(this).val() ) {
                        wpas_get_canned_response_content( $(this).val() )
                }
        });
        
        
        /**
         * Place selected canned response into reply editor once a canned response button is clicked
         */
        $('body').delegate( '.wpas-canned-response', 'click', function(e) {
		e.preventDefault();
                
                
                var response_id = $(this).data('id');
                
                if( response_id ) {
                        wpas_get_canned_response_content( response_id )
                }

	});
        
        
        // add color picker for canned response color
        $('.wpas-cr-color-field').wpColorPicker();

});